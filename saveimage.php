<?php
ini_set('display_errors',0);
ini_set('display_startup_errors', 0);
error_reporting(0);

// createImage.php
if(!trim($_POST["str"])){
  exit(1);
}

$data = base64_decode($_POST["str"]);

$nameImage = generatefilename ();
$appendname = '_back';
if($_POST["front"]){
	$appendname = '_front';
}
$nameImage .= $appendname;  

$urlUploadImages = "image/u/".$nameImage.".png";
 
file_put_contents($urlUploadImages, $data);


echo '["'.$nameImage.'"]'; 
 

function generatefilename (){
	$random_string_length = 10;
	$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
	$string = '';
	$max = strlen($characters) - 1;
	for ($i = 0; $i < $random_string_length; $i++) {
		$string .= $characters[mt_rand(0, $max)];
	}
	return $string;
}