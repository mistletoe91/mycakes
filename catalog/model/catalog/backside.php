<?php
class ModelCatalogBackside extends Model {
	 
	public function getBacksides($data = array()) {
		
		$sql = "SELECT backside_id,name,'' as code,price FROM `".DB_PREFIX."backside` WHERE status = 1";
 
		$re_data = array();

		$query = $this->db->query($sql);

		foreach ($query->rows as $result) {
			$re_data[$result['backside_id']] = $result;
		}

		return $re_data;
	}

	public function getBacksideById($data = array()) {
		
		$sql = "SELECT backside_id,name,code as code,price FROM `".DB_PREFIX."backside` WHERE status = 1 and backside_id=". $data['backsideid'];
 
		$re_data = array();

		$query = $this->db->query($sql);

		foreach ($query->rows as $result) {
			$re_data[$result['backside_id']] = $result;
		}

		return $re_data;
	}

    
}
