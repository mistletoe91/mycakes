<?php
// catalog/controller/api/custom.php
class ControllerApiCustom extends Controller {
  public function products() {
    $this->load->language('api/custom');
    $json = array();
      
    $getProductId = 50;
 
    if (!isset($this->session->data['api_id']) && 0) {
      $json['error']['warning'] = $this->language->get('error_permission');
    } else {
      // load model
      $this->load->model('catalog/product');
 
      // get products 
      $product_info = $this->model_catalog_product->getProduct($getProductId);
      $options = $this->model_catalog_product->getProductOptions($getProductId);
      $product_info ['options'] = $options;
        
      //Get multiple products
      #$products = $this->model_catalog_product->getProducts();
      #$options = $this->model_catalog_product->getProductOptions($products [$getProductId]["product_id"]); 
        
      $json['success']['products'] = $product_info;
    }
     
    if (isset($this->request->server['HTTP_ORIGIN'])) {
      $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
      $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
      $this->response->addHeader('Access-Control-Max-Age: 1000');
      $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    }
 
    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
}