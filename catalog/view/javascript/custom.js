/**
 * Number.prototype.format(n, x)
 * 
 * @param integer n: length of decimal
 * @param integer x: length of sections
 * Source : https://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-money-in-javascript
 */
Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

var basePrice = 0;
var pricelist = {};
pricelist.cakesize = [];
pricelist.cake_filling = [];
pricelist.cake_icing = [];

function appendPriceIfFound (ival){
    if(parseInt(ival.price)>0){
        return ival.name + ' ' + ival.price_prefix+parseInt(ival.price).format(2); 
    } else{
        return ival.name;
    }
}

function updatePrice (){     
    var ctlsizeval = $('#ctlsize').val();
    var finalPrice = basePrice;
    var grossPrice = 0;
    var taxPayable = 0;
    jQuery.each(pricelist.cakesize, function(i, v) { 
        if(ctlsizeval == v.value){            
            finalPrice += parseInt(v.price)    ; 
        }
    }); 
    
    var radio_filling_price = 0;
    $('input[name=radio_filling]:checked').each(function() {
        radio_filling_price = $(this)[0].id; 
    }); 
    jQuery.each(pricelist.cake_filling, function(i, v) { 
        if(radio_filling_price == "radio"+v.value){  
            finalPrice += parseInt(v.price)    ; 
        }
    });     
    
    var radio_icing_price = 0;
    $('input[name=radio_icing]:checked').each(function() {
        radio_icing_price = $(this)[0].id; 
    });
    jQuery.each(pricelist.cake_icing, function(i, v) { 
        if(radio_icing_price == "radio"+v.value){            
            finalPrice += parseInt(v.price)    ; 
        }
    });     
    
    $('.placeholder_finalprice').html('$'+finalPrice.format(2)+'<small>+ HST</small>');
    
    taxPayable = finalPrice*0.13;
    grossPrice = (finalPrice + taxPayable).format(2) ;
    
    $('#payment_finalprice').html('$'+finalPrice.format(2)+' + $'+taxPayable+' (HST) = <strong>$'+grossPrice+'</strong>');
    $('#payment_btn').html('Pay $'+grossPrice); 
    
}


$( document ).ready(function() {
      
     
    //Get Main Product Information
    $.ajax({
			url: '/api/getproductbyid.php',
			type: 'post',
			data: 'product_id=none',
			dataType: 'json',
			beforeSend: function() {
				//$('#cart > button').button('loading');
			},
			complete: function() {
				//$('#cart > button').button('reset');
			},
			success: function(json) { 
				//$('.alert, .text-danger').remove();  
				//if (json['success']) {
					basePrice = parseInt(json['success']['products']['price']);
                    //console.log (item_price.format(2));
                
                    var json_options = json['success']['products']['options'];                    
                    jQuery.each(json_options, function(i, val) {
                        
                        if (val.product_option_id == "242"){    
                            pricelist.cakesize = [];
                            
                            //Cake Size 
                            jQuery.each(val.product_option_value, function(ii, ival) {
                                $('#ctlsize').append($("<option/>", {
                                        value: ival.product_option_value_id,
                                        text: appendPriceIfFound(ival)
                                })).niceSelect('update');;    
                                
                                var cakesizearr = {
                                    "value": ival.product_option_value_id,
                                    "price": ival.price,
                                }
                                pricelist.cakesize.push(cakesizearr);                                
                                
                            });  
                            
                            $(document).on("change", "#ctlsize", function(){ 
                                updatePrice ()  ;
                            });                             
                            
                        }//endif
                        
                        if (val.product_option_id == "238"){                            
                            //Cake Type 
                            jQuery.each(val.product_option_value, function(ii, ival) {
                                $('#ctltype').append($("<option/>", {
                                        value: ival.product_option_value_id,
                                        text: appendPriceIfFound(ival)
                                })).niceSelect('update');;                                
                            });                            
                        }//endif
                        if (val.product_option_id == "236"){                            
                            //Cake Color 
                            jQuery.each(val.product_option_value, function(ii, ival) {
                                $('#ctlcolor').append($("<option/>", {
                                        value: ival.product_option_value_id,
                                        text: appendPriceIfFound(ival)
                                })).niceSelect('update');;                                
                            });                            
                        }//endif
                        if (val.product_option_id == "000"){                            
                            //Cake Decoration 
                            jQuery.each(val.product_option_value, function(ii, ival) {
                                $('#ctldecoration').append($("<option/>", {
                                        value: ival.product_option_value_id,
                                        text: appendPriceIfFound(ival)
                                })).niceSelect('update');;                                
                            });                            
                        }//endif    
                        
                        if (val.product_option_id == "235"){                            
                            //Cake Filling 
                            pricelist.cake_filling = [];
                            var clsfilling1='';
                            var clsfilling2='';
                            var clsfilling3='';
                            var clsfilling1_cnt=0;
                            var clsfilling2_cnt=0;
                            var clsfilling3_cnt=0;
                            var clsfilling_total_cnt=0;
                            
                            jQuery.each(val.product_option_value, function(ii, ival) {
                                    clsfilling_total_cnt++; 
                                    var cakearr = {
                                        "value": ival.product_option_value_id,
                                        "price": ival.price,
                                    }
                                    pricelist.cake_filling.push(cakearr);                                   
                            });                            
                            
                            jQuery.each(val.product_option_value, function(ii, ival) {    
                                    
                                    if(clsfilling1_cnt++ < Math.ceil(clsfilling_total_cnt/3)){  
                                        clsfilling1 += '<div class="funkyradio-primary"><input type="radio" name="radio_filling" id="radio'+ival.product_option_value_id+'" /><label for="radio'+ival.product_option_value_id+'">'+appendPriceIfFound(ival)+'</label></div>';
                                    } else {
                                        if(clsfilling2_cnt++ < Math.ceil(clsfilling_total_cnt/3)){ 
                                            clsfilling2 += '<div class="funkyradio-primary"><input type="radio" name="radio_filling" id="radio'+ival.product_option_value_id+'" /><label for="radio'+ival.product_option_value_id+'">'+appendPriceIfFound(ival)+'</label></div>';       
                                        } else {
                                            clsfilling3 += '<div class="funkyradio-primary"><input type="radio" name="radio_filling" id="radio'+ival.product_option_value_id+'" /><label for="radio'+ival.product_option_value_id+'">'+appendPriceIfFound(ival)+'</label></div>';       
                                        }//endif
                                    }//endif                       
                            });                                 
                            
                            $('.clsfilling1').html(clsfilling1);
                            $('.clsfilling2').html(clsfilling2);
                            $('.clsfilling3').html(clsfilling3);
                            
                            $(document).on("change", "#divaccordion_2 input:radio", function(){
                                    updatePrice ();
                                    fnradioClickDivAccordion_ (this, 2, 3);                                    
                            });                            
                            
                        }//endif   
                                                
                        if (val.product_option_id == "237"){                            
                            //Cake Icing 
                            pricelist.cake_icing = [];
                            var clsfilling1='';
                            var clsfilling2='';
                            var clsfilling3='';
                            var clsfilling1_cnt=0;
                            var clsfilling2_cnt=0;
                            var clsfilling3_cnt=0;
                            var clsfilling_total_cnt=0;
                            
                            jQuery.each(val.product_option_value, function(ii, ival) {
                                    clsfilling_total_cnt++;   
                                    var cakearr = {
                                        "value": ival.product_option_value_id,
                                        "price": ival.price,
                                    }
                                    pricelist.cake_icing.push(cakearr);                                    
                            });                            
                            
                            jQuery.each(val.product_option_value, function(ii, ival) {    
                                    
                                    if(clsfilling1_cnt++ < Math.ceil(clsfilling_total_cnt/3)){  
                                        clsfilling1 += '<div class="funkyradio-primary"><input type="radio" name="radio_icing" id="radio'+ival.product_option_value_id+'" /><label for="radio'+ival.product_option_value_id+'">'+appendPriceIfFound(ival)+'</label></div>';
                                    } else {
                                        if(clsfilling2_cnt++ < Math.ceil(clsfilling_total_cnt/3)){ 
                                            clsfilling2 += '<div class="funkyradio-primary"><input type="radio" name="radio_icing" id="radio'+ival.product_option_value_id+'" /><label for="radio'+ival.product_option_value_id+'">'+appendPriceIfFound(ival)+'</label></div>';       
                                        } else {
                                            clsfilling3 += '<div class="funkyradio-primary"><input type="radio" name="radio_icing" id="radio'+ival.product_option_value_id+'" /><label for="radio'+ival.product_option_value_id+'">'+appendPriceIfFound(ival)+'</label></div>';       
                                        }//endif
                                    }//endif                       
                            });                                 
                            
                            $('.clsicing1').html(clsfilling1);
                            $('.clsicing2').html(clsfilling2);
                            $('.clsicing3').html(clsfilling3);
                            
                            $(document).on("click", "#divaccordion_3 input:radio", function(){
                                    updatePrice ();
                                    fnradioClickDivAccordion_ (this, 3, 7);
                            });                            
                            
                        }//endif -- icing
                        
                    });
                    //ctlsize
				//}//endif
                
                updatePrice ();
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
    }); 
    
    $('select').niceSelect();
    
$('#divaccordion_3').animate({
    scrollTop: $('#divaccordion_3').get(0).scrollHeight
}, 1500);

    $("#accordion").sticky({ topSpacing: 50 });
     $('[data-toggle="tooltip"]').tooltip();
    
   
    function getStringToPutInTitle ($id){
       var $initVal = $('#'+$id).html(); 
        if($initVal.indexOf(":")>0){
           $initVal = $initVal.split(':') [0]; 
        } else {
           $initVal += " " ;
        } 
        return $initVal;
    }
     
    function beautifymyString ($id,new_s){
        var old = getStringToPutInTitle($id);       
        var str = old + ": "+ "<small>"+new_s+"</small>";
        return str;        
    }
     
    
    function fnradioClickDivAccordion_ (dis, oldid, newid, beautify = 1){
        $('#divaccordion_'+newid).show ();
        $('#collapse1_'+newid+'_btn').click ();  
        if(beautify>0){
            $('#collapse1_'+oldid+'_btn').html ( beautifymyString('collapse1_'+oldid+'_btn', $(dis).next().html()) );         
        }
    }
 
    
    
	$( ".href_1" ).click(function() {
        $('#divaccordion_2').show ();
        $('#collapse1_2_btn').click ();  
         
        $(".href_1").css('color', '#337ab7');
        $(this).css('color', '#23527c');
        
 
      //  $('#collapse1_1_btn').html ( beautifymyString('collapse1_1_btn', $(this).next().html()) );
	}); 
    
        
   
	$( "#divaccordion_6 input:radio"  ).change(function() {
        fnradioClickDivAccordion_ (this, 6, 7);
	});   
      
	$( "#href_delivery_details" ).click(function() {
        fnradioClickDivAccordion_ (this, 0, 8, 0);
	});
    
      
	$( "#href_payment_details" ).click(function() {
        fnradioClickDivAccordion_ (this, 0, 9, 0);
	});
        
    /*
    POSTAL CODE  code
    $("#txtpostalcode").keyup(function(){
      var temp = $(this).val().toUpperCase(); 
      temp = temp.replace(/\-/g, ''); 
      if (temp.length > 3 ){
         $(this).val(temp.substring (0,3) + "-" + temp.substring (3,temp.length));
      } else {
         $(this).val(temp);
      }
    });
    */
    
    //hide other collapse 
    /*
    $('.collapse').on('show.bs.collapse', function () {
        $('.collapse.in').each(function(){
            //$(this).collapse('hide');
        });
    });    
    */
 

    
});