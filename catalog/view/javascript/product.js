var bleedline1;
var bleedline2;
var canvas;

/*
Number.prototype.round = function(p) {
  p = p || 10; 
  return parseFloat( this.toFixed(p) ); 
};
*/

$( document ).ready(function() {
	  
    $('.placeholder_discount').on('change', function() {  
    	  var arr = this.value.split('_');
          
          var raw_price = Number(arr[1].replace(/[^0-9\.]+/g,""));
          var raw_qty = Number(arr[0].replace(/[^0-9\.]+/g,"")); 
        
    	  $('.placeholder_finalprice'). html ('$' + parseFloat(parseFloat(raw_price)*parseFloat(raw_qty)).toFixed(2) ); 
    	  $('#input-quantity').val (arr[0]);
    	  $('.placeholder_discount'). val(this.value); 
    });
    
    canvas = new fabric.Canvas('canvas-product-front', {
        preserveObjectStacking: true
    });

    	 

	$( ".clsbacksavebtn,.clsgetfinalproof" ).click(function() {
		var json = canvas.toJSON();
		//$( "#input-option227" ).val(JSON.stringify(json));   
	});	     
		
	//Check if the data is already saved
    var jsondefault = $('#input-option230').val();
	var myjson_front = '';
    //myjson_front = $( "#input-option227" ).val();
	
	if(myjson_front.replace(/\s/g,"") == ""){
		//Get from detault
		canvas.clear();
		canvas.loadFromJSON(jsondefault, canvas.renderAll.bind(canvas)); 
	} else {
		//User has something entered in front
		canvas.clear();
		canvas.loadFromJSON(myjson_front, canvas.renderAll.bind(canvas)); 
	} 
    
	//var canvas = new fabric.Canvas('canvas-product-front');
	canvas.setWidth(700);
	canvas.setHeight(500);	
	 
    
	//File upload
	document.getElementById('btnfileupload').addEventListener("change", function (e) {
		  var file = e.target.files[0];
		  var reader = new FileReader();
		  reader.onload = function (f) {
		    var data = f.target.result;                    
		    fabric.Image.fromURL(data, function (img) {
		      var oImg = img.set({left: 0, top: 0, angle: 00}).scale(0.9);
		      canvas.add(oImg).renderAll();
		      var a = canvas.setActiveObject(oImg);
		      var dataURL = canvas.toDataURL({format: 'png', quality: 0.8});
		    });
		  };
		  reader.readAsDataURL(file);
	});
	
	var selectedObject; 
	
	$( "#sendSelectedObjectToFront" ).click(function() {
		canvas.bringToFront(selectedObject);
	});	
	
	$( "#sendSelectedObjectBack" ).click(function() {
		canvas.sendToBack(selectedObject);
	});	
	
	$( "#fileuploadbtn" ).click(function() {
		$( "#btnfileupload" ).click();
	});		
	
	$( "#savebtn" ).click(function() {

		
        $('ul.setup-panel li:eq(1)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-2"]').trigger('click');		
		
		//$('#divproof').slideDown ();
		var json = canvas.toJSON();
		//$( "#input-option227" ).val(JSON.stringify(json));        
        
	});			
	
	
	$( "#addnewtext" ).click(function() {
		canvas.add(new fabric.IText('Tap and Type', { 
			  fontFamily: 'Arial',
			  left: 100, 
			  top: 100 ,
			}));
	});	
	
	// select all objects
	function deleteObjects(){
		var activeObject = canvas.getActiveObject(),
	    activeGroup = canvas.getActiveGroup();
	    if (activeObject) {
	        if (confirm('Are you sure?')) {
	            canvas.remove(activeObject);
	        }
	    }
	    else if (activeGroup) {
	        if (confirm('Are you sure?')) {
	            var objectsInGroup = activeGroup.getObjects();
	            canvas.discardActiveGroup();
	            objectsInGroup.forEach(function(object) {
	            canvas.remove(object);
	            });
	        }
	    }
	}
	
	$("#deletebtn").click(function(){
	    canvas.isDrawingMode = false;
	    deleteObjects();
	});
	 
	//Bind buttons for Text editor
	$( "#btnleftalign" ).click(function() {
		$( "#text-cmd-bold" ).click(); 
		$('#text-align'). val ('left').change();   
		
		$( this ).toggleClass ('active');
		if ( $( this ).hasClass( "active" ) ) {
			$( "#btncentertalign" ).removeClass ('active');
			$( "#btnrightalign" ).removeClass ('active'); 
		} 
	});
	$( "#btncentertalign" ).click(function() {
		$( "#text-cmd-bold" ).click(); 
		$('#text-align'). val ('center').change();   
		
		
		$( this ).toggleClass ('active');
		if ( $( this ).hasClass( "active" ) ) {		
			$( "#btnleftalign" ).removeClass ('active');
			$( "#btnrightalign" ).removeClass ('active');
		}
	});
	$( "#btnrightalign" ).click(function() {
		$( "#text-cmd-bold" ).click(); 
		$('#text-align'). val ('right').change();  
		
		
		$( this ).toggleClass ('active');
		if ( $( this ).hasClass( "active" ) ) {		
			$( "#btnleftalign" ).removeClass ('active');
			$( "#btncentertalign" ).removeClass ('active');
		}
	});
	
	$( "#btnbold" ).click(function() {
		$( "#text-cmd-bold" ).click();   
		$( this ).toggleClass ('active');
	});
	$( "#btnitalic" ).click(function() {
		$( "#text-cmd-italic" ).click(); 
		$( this ).toggleClass ('active');	
	});
	
	$( "#btnunderline" ).click(function() {
		$( "#text-cmd-underline" ).click();
		$( this ).toggleClass ('active');
		if ( $( this ).hasClass( "active" ) ) {		
			$( "#btnstrikethrough" ).removeClass ('active'); 
		}			
	});
	$( "#btnstrikethrough" ).click(function() {
		$( "#text-cmd-linethrough" ).click();
		$( this ).toggleClass ('active');
		if ( $( this ).hasClass( "active" ) ) {		
			$( "#btnunderline" ).removeClass ('active'); 
		}				
	});
	

	
	function onObjectSelected(e) {
		  selectedObject = e.target;
		  var type= e.target.get('type');
		  //console.log (type);
		  if(type==="i-text"){ 		  
                 //Text    			  
                 jQuery("#overlaycanvas").css( 
                		 "left",  e.target.getLeft()  
                 );
                 jQuery("#overlaycanvas").css( 
                		 "top",  e.target.getTop() -100
                 );		 
                 jQuery("#text-wrapper").detach().appendTo('#overlaycanvas'); 
                  
                  
                
                 
                 if(e.target.fontWeight.toLowerCase() === 'bold'){
                	 $( "#btnbold" ).addClass ('active');
                 } else {
                	 $( "#btnbold" ).removeClass ('active');
                 }
                 if(e.target.fontStyle.toLowerCase() === 'italic'){
                	 $( "#btnitalic" ).addClass ('active');
                 } else {
                	 $( "#btnitalic" ).removeClass ('active');
                 }   
                 if(e.target.textDecoration.toLowerCase() === 'underline'){
                	 $( "#btnunderline" ).addClass ('active');
                 } else {
                	 $( "#btnunderline" ).removeClass ('active');
                 }  
                 if(e.target.textDecoration.toLowerCase() === 'line-through'){
                	 $( "#btnstrikethrough" ).addClass ('active');
                 } else {
                	 $( "#btnstrikethrough" ).removeClass ('active'); 
                 }                         
                 
                  
                 
                 if(e.target.textAlign.toLowerCase() === 'left'){
                	 $( "#btnleftalign" ).addClass ('active');
                 } else {
                	 $( "#btnleftalign" ).removeClass ('active');
                 }   
                 if(e.target.textAlign.toLowerCase() === 'center'){
                	 $( "#btncentertalign" ).addClass ('active');
                 } else {
                	 $( "#btncentertalign" ).removeClass ('active');
                 }     
                 if(e.target.textAlign.toLowerCase() === 'right'){
                	 $( "#btnrightalign" ).addClass ('active');
                 } else {
                	 $( "#btnrightalign" ).removeClass ('active');
                 }                      
                                  
                 
                 //Set the values of text editor
                 $('#text-font-size').val (e.target.fontSize);
                 $('#font-family').val (e.target.fontFamily.toLowerCase()); 
                 $('#text-align').val (e.target.textAlign.toLowerCase()).change();
                 
                 $('#text-color').val (e.target.fill.toLowerCase());
                 $('#text-bg-color').val (e.target.backgroundColor .toLowerCase());
	      } else {
                //Everything else
	    	    
	    	  //Move the text one back
	    	  jQuery("#text-wrapper").detach().appendTo('#placeholdercanvas'); 
	      }
	}
	
	function onObjectSelectedCleared(e) {
		  var type= e.target.get('type');
		   
		  if(type==="i-text"){ 		  
               //Text     
			  jQuery("#text-wrapper").detach().appendTo('#placeholdercanvas'); 
                 
	      } else {
              //Everything else
	    	    
	    	  //Move the text one back
	    	  jQuery("#text-wrapper").detach().appendTo('#placeholdercanvas'); 
	      }
	}	
	
	canvas.on('object:selected', onObjectSelected);
	canvas.on('selection:cleared', onObjectSelectedCleared);
		
	/*
	
	var imgHREF = $('.thumbnail').attr('href'); 
	 
	imgHREF = imgHREF.replace('/cache/', '/');
	imgHREF = imgHREF.replace('-500x500', '');
	 
	// Locked backgrond
	//canvas.setBackgroundImage(imgHREF, canvas.renderAll.bind(canvas));
	
 
	  var rect2 = new fabric.Rect({
		    width: 680, height: 480, left: 10, top: 10,
		    stroke: '#999', strokeWidth: 1,strokeDashArray: [5, 5],
		    fill: 'rgba(0,0,0,0)'
		    ,lockMovementX  : true 
		    ,lockMovementY  : true
		    ,lockScalingX  : true
		    ,lockScalingY  : true
		    ,lockRotation : true
		    ,hasControls  : false
		    ,selectable : false
	  });
	  //canvas.add(rect2);
	  
	    
	  
     // canvas.sendToBack(object);

  	var text40 = new fabric.IText("MAIN HEADING", {
		  fontSize: 40, left : 180, top : 100
		});
	var text20 = new fabric.IText("Sub Heading", {
		  fontSize: 20, left: 200, top : 200
		});
		
	//canvas.add(text40);
	//canvas.add(text20);
	 
	
	//Some controls and stuff
	var rect = new fabric.Rect({
	    left: 150,
	    top: 200,
	    originX: 'left',
	    originY: 'top',
	    width: 150,
	    height: 120,
	    angle: -10,
	    fill: 'rgba(255,0,0,0.5)',
	    transparentCorners: false
	  });

	  //canvas.add(rect).setActiveObject(rect);
	*/

	  function observeBoolean(property) { 
	    document.getElementById(property).onclick = function() { 
	      canvas.getActiveObject()[property] = this.checked;
	      canvas.renderAll();
	    };
	  }

	  function observeNumeric(property) {
	    document.getElementById(property).onchange = function() {
	      canvas.getActiveObject()[property] = this.value;
	      canvas.renderAll();
	    };
	  }

	  function observeOptionsList(property) {
	    var list = document.querySelectorAll('#' + property + 
	    ' [type="checkbox"]');
	    for (var i = 0, len = list.length; i < len; i++) {
	      list[i].onchange = function() {
	        canvas.getActiveObject()[property](this.name, this.checked);
	        canvas.renderAll();
	      };
	    };
	  }
	  observeNumeric('padding'); 
	  
      //Text fields
	  
	  document.getElementById('text-color').onchange = function() {
          canvas.getActiveObject().setFill(this.value);
          canvas.renderAll();
      };
      document.getElementById('text-color').onchange = function() {
          canvas.getActiveObject().setFill(this.value);
          canvas.renderAll();
      };
		
		document.getElementById('text-bg-color').onchange = function() {
          canvas.getActiveObject().setBackgroundColor(this.value);
          canvas.renderAll();
      };
		
		document.getElementById('text-lines-bg-color').onchange = function() {
          canvas.getActiveObject().setTextBackgroundColor(this.value);
          canvas.renderAll();
      };

		document.getElementById('text-stroke-color').onchange = function() {
          canvas.getActiveObject().setStroke(this.value);
          canvas.renderAll();
      };	

		document.getElementById('text-stroke-width').onchange = function() {
          canvas.getActiveObject().setStrokeWidth(this.value);
          canvas.renderAll();
      };				
	
		document.getElementById('font-family').onchange = function() {
          canvas.getActiveObject().setFontFamily(this.value);
          canvas.renderAll();
      };
		
		document.getElementById('text-font-size').onchange = function() {
          canvas.getActiveObject().setFontSize(this.value);
          canvas.renderAll();
      };
		
		document.getElementById('text-line-height').onchange = function() {
          canvas.getActiveObject().setLineHeight(this.value);
          canvas.renderAll();
      };
		
		document.getElementById('text-align').onchange = function() {
          canvas.getActiveObject().setTextAlign(this.value);
          canvas.renderAll();
      }; 
radios5 = document.getElementsByName("fonttype");  // wijzig naar button
  for(var i = 0, max = radios5.length; i < max; i++) {
      radios5[i].onclick = function() {
          
          if(document.getElementById(this.id).checked == true) {
              if(this.id == "text-cmd-bold") {
                  canvas.getActiveObject().set("fontWeight", "bold");
              }
              if(this.id == "text-cmd-italic") {
                  canvas.getActiveObject().set("fontStyle", "italic");
              }
              if(this.id == "text-cmd-underline") {
                  canvas.getActiveObject().set("textDecoration", "underline");
              }
				if(this.id == "text-cmd-linethrough") {
                  canvas.getActiveObject().set("textDecoration", "line-through");
              }
				if(this.id == "text-cmd-overline") {
                  canvas.getActiveObject().set("textDecoration", "overline");
              }
              
              
              
          } else {
              if(this.id == "text-cmd-bold") {
                  canvas.getActiveObject().set("fontWeight", "");
              }
              if(this.id == "text-cmd-italic") {
                  canvas.getActiveObject().set("fontStyle", "");
              }  
              if(this.id == "text-cmd-underline") {
                  canvas.getActiveObject().set("textDecoration", "");
              }
				if(this.id == "text-cmd-linethrough") {
                  canvas.getActiveObject().set("textDecoration", "");
              }  
              if(this.id == "text-cmd-overline") {
                  canvas.getActiveObject().set("textDecoration", "");
              }
          }
          
          
          canvas.renderAll();
      }
  }
	 

});

