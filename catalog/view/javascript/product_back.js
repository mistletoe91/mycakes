var bleedline3;
var bleedline4;
var backside;
 

$( document ).ready(function() {
    var backcanvas;
    var frontcanvas_again ;
    var productid = 1;
    $.ajax({
    	 type: 'GET',
    	 url : "/index.php?route=api/getbacksidetemplatesbyproductid/backside",
    	 data: { productid : productid },
    	 dataType: 'json',
         success: function (data) {  
                    backside = data;
             
                    //Check if data already saved
                    var jsondefault = $('#input-option231').val();   
             
                	doint (jsondefault, 1);
         }
    });

    function loadbacktemplate (id){  
    $.ajax({
    	 type: 'POST',
    	 url : "/index.php?route=api/getbacksidetemplatesbyproductid/backside",
    	 data: { backsideid: id },
    	 dataType: 'json',
         success: function (data) {  
               $.each(backside.success.backsides, function (key, val) { 
                    if(key === id){ 
                        backside.success.backsides[key].code =  data.success.backsides[key].code ;  
                    }  
               });
               doint (id, 0);       
                      
         }
    });         
        
    }//end function   

    
	  function observeBoolean(property) { 
	    document.getElementById(property).onclick = function() { 
	      backcanvas.getActiveObject()[property] = this.checked;
	      backcanvas.renderAll();
	    };
	  }

	  function observeNumeric(property) {
	    document.getElementById(property).onchange = function() {
	      backcanvas.getActiveObject()[property] = this.value;
	      backcanvas.renderAll();
	    };
	  }

	  function observeOptionsList(property) {
	    var list = document.querySelectorAll('#' + property + 
	    ' [type="checkbox"]');
	    for (var i = 0, len = list.length; i < len; i++) {
	      list[i].onchange = function() {
	        backcanvas.getActiveObject()[property](this.name, this.checked);
	        backcanvas.renderAll();
	      };
	    };
	  }

function onObjectSelected_back(e) {  
		  selectedObject = e.target;
		  var type= e.target.get('type');
		   
		  if(type==="i-text"){ 		  
                 //Text    			  
                 jQuery("#backoverlaycanvas").css( 
                		 "left",  e.target.getLeft()  
                 );
                 jQuery("#backoverlaycanvas").css( 
                		 "top",  e.target.getTop() -100
                 );		 
                 jQuery("#backtext-wrapper").detach().appendTo('#backoverlaycanvas'); 
                  
                  
                
                 
                 if(e.target.fontWeight.toLowerCase() === 'bold'){
                	 $( "#backbtnbold" ).addClass ('active');
                 } else {
                	 $( "#backbtnbold" ).removeClass ('active');
                 }
                 if(e.target.fontStyle.toLowerCase() === 'italic'){
                	 $( "#backbtnitalic" ).addClass ('active');
                 } else {
                	 $( "#backbtnitalic" ).removeClass ('active');
                 }    
                 if(e.target.textDecoration.toLowerCase() === 'underline'){
                	 $( "#backbtnunderline" ).addClass ('active');
                 } else {
                	 $( "#backbtnunderline" ).removeClass ('active');
                 }  
                 if(e.target.textDecoration.toLowerCase() === 'line-through'){
                	 $( "#backbtnstrikethrough" ).addClass ('active');
                 } else {
                	 $( "#backbtnstrikethrough" ).removeClass ('active'); 
                 }                         
                 
                  
                 
                 if(e.target.textAlign.toLowerCase() === 'left'){
                	 $( "#backbtnleftalign" ).addClass ('active');
                 } else {
                	 $( "#backbtnleftalign" ).removeClass ('active');
                 }   
                 if(e.target.textAlign.toLowerCase() === 'center'){
                	 $( "#backbtncentertalign" ).addClass ('active');
                 } else {
                	 $( "#backbtncentertalign" ).removeClass ('active');
                 }     
                 if(e.target.textAlign.toLowerCase() === 'right'){
                	 $( "#backbtnrightalign" ).addClass ('active');
                 } else {
                	 $( "#backbtnrightalign" ).removeClass ('active');
                 }                      
                                  
                 
                 //Set the values of text editor
                 $('#backtext-font-size').val (e.target.fontSize);
                 $('#backfont-family').val (e.target.fontFamily.toLowerCase()); 
                 $('#backtext-align').val (e.target.textAlign.toLowerCase()).change();
                 
                 $('#backtext-color').val (e.target.fill.toLowerCase());
                 $('#backtext-bg-color').val (e.target.backgroundColor .toLowerCase());
	      } else {
                //Everything else
	    	    putbacktheeditor ();    

	      }
	}
    
    function putbacktheeditor (){
	    	  //Move the text one back
	    	  jQuery("#backtext-wrapper").detach().appendTo('#backplaceholdercanvas');         
    }
	
	function onObjectSelectedCleared_back(e) {
		  var type= e.target.get('type'); 
		  putbacktheeditor ();
          return ;
        
		  if(type==="i-text"){ 		  
               //Text     
			  jQuery("#backtext-wrapper").detach().appendTo('#backplaceholdercanvas'); 
                 
	      } else {
              //Everything else
	    	    
	    	  //Move the text one back
	    	  jQuery("#backtext-wrapper").detach().appendTo('#backplaceholdercanvas'); 
	      }
	}	    

function sendStringRepresentation(front){ 
  
    	canvas.deactivateAll().renderAll();
    	backcanvas.deactivateAll().renderAll();
    	
    	var strDataURI; 
    	if(front>0){
    		//strDataURI = canvas.toDataURL(); 
    		strDataURI = frontcanvas_again.toDataURL();   
    	} else {
    		strDataURI = backcanvas.toDataURL();    		
    		var json = backcanvas.toJSON();
    		$( "#input-option229" ).val(JSON.stringify(json));       		
    	}
        
        strDataURI = strDataURI.substr(22, strDataURI.length); 
        $.post("/saveimage.php",{str: strDataURI, front : front },
                function(data){
           
        	
                	var obj = jQuery.parseJSON( data);
                	if(obj [0]) { 
                		if(front>0){
                			$('#'+$id_of_fileoutput_front). val(obj [0]);
                			
                			//Add image for proof
                            $('#clsproof_front > img').attr('src','/image/u/'+obj [0]+'.png');
                            
                		} else {
                			$('#'+$id_of_fileoutput_back). val(obj [0]);

                			//Add image for proof
                            $('#clsproof_back > img').attr('src','/image/u/'+obj [0]+'.png');
                			
                		}//endif
                	}//endif
                	
                	
        });
        
    }//end function 

    	// select all objects
	function deleteObjects(){
		var activeObject = backcanvas.getActiveObject(),
	    activeGroup = backcanvas.getActiveGroup();
	    if (activeObject) {
	        if (confirm('Are you sure?')) {
	            backcanvas.remove(activeObject);
	        }
	    }
	    else if (activeGroup) {
	        if (confirm('Are you sure?')) {
	            var objectsInGroup = activeGroup.getObjects();
	            backcanvas.discardActiveGroup();
	            objectsInGroup.forEach(function(object) {
	            backcanvas.remove(object);
	            });
	        }
	    }
	}

    function bleedlines (cnvs, pad, color){ 
    	                           var line1 = new fabric.Line([
    	                             pad, 0,
    	                             pad, cnvs.height
    	                           ],{ 
    	                             stroke: color,strokeDashArray: [5, 5] 
    	                           })
    	                           var line2 = new fabric.Line([
    	                             cnvs.width - pad, 0,
    	                             cnvs.width - pad, cnvs.height
    	                           ],{ 
    	                             stroke: color, strokeDashArray: [5, 5]
    	                           })
    	                           var line3 = new fabric.Line([
    	                             0, pad,
    	                             cnvs.width, pad
    	                           ],{ 
    	                             stroke: color,strokeDashArray: [5, 5] 
    	                           })
    	                           var line4 = new fabric.Line([
    	                             0, cnvs.height - pad,
    	                             cnvs.width, cnvs.height - pad
    	                           ],{ 
    	                             stroke: color, strokeDashArray: [5, 5]
    	                           });

    	                           var group = new fabric.Group([
    	                           	line1,
    	                             line2,
    	                             line3,
    	                             line4
    	                           ]);
    	                           group.selectable = false;
    	                           group.evented = false;

    	                           cnvs.add(group);
 
    	                           group.bringToFront();
    	                           return group;  
    }//end function	 aa
    


function doint (jsondefault, virgin){
    $('#div_backcanvas-product-front').html ('<canvas id="backcanvas-product-front"></canvas>');
    putbacktheeditor ();    
    
	$id_of_fileoutput_back = ''; 
	$id_of_fileoutput_front = ''; 
	//Find all hidden texts
    $('input[type="text"]').each(function() {   
        if($(this).attr('placeholder') == 'fileoutput_back'){ 
        	$id_of_fileoutput_back = $(this).attr('id');
        }//endif
        if($(this).attr('placeholder') == 'fileoutput_front'){ 
        	$id_of_fileoutput_front = $(this).attr('id');
        }//endif          
    });  
    
        
    backcanvas = new fabric.Canvas('backcanvas-product-front', {
        preserveObjectStacking: true
    }); 
    frontcanvas_again = document.getElementById("canvas-product-front"); 
 
	//Check if the data is already saved 
	//var myjson_back = $( "#input-option229" ).val();
    var myjson_back = '';
    myjson_back = $( "#input-option229" ).val();
    
    $( ".backsidetemplates > .row" ).empty(); 
	//Get the backside template from jsondefault 
    $.each(backside.success.backsides, function (key, val) {  
    	if(!jsondefault){
    		jsondefault = 1;//just use default one 
    	}
         
        var price = 0;
        if((Math.round( val.price * 100 )/100) != 0){
            price = (Math.round( val.price * 100 )/100).toFixed(2);
        }//endif
         
        if(!price || price === "0.00" || price == 0){
            price = 'FREE';
        } else {
            price = '$'+price;
        }
        $('.backsidetemplates > .row').append ('<div class="col-md-4"><a href="#" id="loadbacktemplate'+val.backside_id+'"><img style="width:100px;height:100px;" src="/image/backside/'+val.backside_id+'.png" alt="'+val.name+'" class="img-thumbnail" /> <p>'+price+'</p></a></div>');
        
        $('#loadbacktemplate'+val.backside_id ).click( function(e) {e.preventDefault(); loadbacktemplate(val.backside_id);  return false; } ); 

        	//<div class="img-rounded" >'+val.name+'</div>');

    	if(key ===jsondefault){
    		 jsondefault = val.code; 
    	}//endif
    		   
	})
	
	if(myjson_back.replace(/\s/g,"") == ""){
		//Get from detault
		backcanvas.clear();
		backcanvas.loadFromJSON(jsondefault, backcanvas.renderAll.bind(backcanvas)); 
	} else {
		//User has something entered in front
		backcanvas.clear();
		backcanvas.loadFromJSON(myjson_back, backcanvas.renderAll.bind(backcanvas));
	} 
    
	//var backcanvas = new fabric.Canvas('backcanvas-product-front');
	backcanvas.setWidth(700);
	backcanvas.setHeight(500);	
	
    bleedline1 = bleedlines (canvas, 10, '#FF4500');     
    bleedline2 = bleedlines (canvas, 20, '#000');    
    bleedline3 = bleedlines (backcanvas, 10, '#FF4500');     
    bleedline4 = bleedlines (backcanvas, 20, '#000');

	  
	//File upload
	document.getElementById('backbtnfileupload').addEventListener("change", function (e) {
		  var file = e.target.files[0];
		  var reader = new FileReader();
		  reader.onload = function (f) {
		    var data = f.target.result;                    
		    fabric.Image.fromURL(data, function (img) {
		      var oImg = img.set({left: 0, top: 0, angle: 00}).scale(0.9);
		      backcanvas.add(oImg).renderAll();
		      var a = backcanvas.setActiveObject(oImg);
		      var dataURL = backcanvas.toDataURL({format: 'png', quality: 0.8});
		    });
		  };
		  reader.readAsDataURL(file);
	});
	
	var selectedObject; 
    
    if(virgin) { 
         
    //Avoid creating duplicates    
	$( "#backaddnewtext" ).click(function() {
		backcanvas.add(new fabric.IText('Click and Type', { 
			  fontFamily: 'Arial',
			  left: 100, 
			  top: 100 ,
			}));
	});	
        
	
	$( "#backsendSelectedObjectToFront" ).click(function() {
		backcanvas.bringToFront(selectedObject);
	});	
	
	$( "#backsendSelectedObjectBack" ).click(function() {
		backcanvas.sendToBack(selectedObject);
	});	
	
	$( "#backfileuploadbtn" ).click(function() {
		$( "#backbtnfileupload" ).click();
	});		
        
	$( "#backsavebtn" ).click(function() { 
        $('ul.setup-panel li:eq(2)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-3"]').trigger('click');  
	});		
	
	$( 'ul.setup-panel li a[href="#step-3"]' ).click(function() {
		sendStringRepresentation  (0);
        sendStringRepresentation  (1);		
	});
	  
	$("#backdeletebtn").click(function(){
	    backcanvas.isDrawingMode = false;
	    deleteObjects();
	});        
	        
	//Bind buttons for Text editor
	$( "#backbtnleftalign" ).click(function() {
		$( "#backtext-cmd-bold" ).click(); 
		$('#backtext-align'). val ('left').change();   
		
		$( this ).toggleClass ('active');
		if ( $( this ).hasClass( "active" ) ) {
			$( "#backbtncentertalign" ).removeClass ('active');
			$( "#backbtnrightalign" ).removeClass ('active'); 
		} 
	});
	$( "#backbtncentertalign" ).click(function() {
		$( "#backtext-cmd-bold" ).click(); 
		$('#backtext-align'). val ('center').change();   
		
		
		$( this ).toggleClass ('active');
		if ( $( this ).hasClass( "active" ) ) {		
			$( "#backbtnleftalign" ).removeClass ('active');
			$( "#backbtnrightalign" ).removeClass ('active');
		}
	});
	$( "#backbtnrightalign" ).click(function() {
		$( "#backtext-cmd-bold" ).click(); 
		$('#backtext-align'). val ('right').change();  
		
		
		$( this ).toggleClass ('active');
		if ( $( this ).hasClass( "active" ) ) {		
			$( "#backbtnleftalign" ).removeClass ('active');
			$( "#backbtncentertalign" ).removeClass ('active');
		}
	});
	
	$( "#backbtnbold" ).click(function() {
		$( "#backtext-cmd-bold" ).click();   
		$( this ).toggleClass ('active');
	});
	$( "#backbtnitalic" ).click(function() {
		$( "#backtext-cmd-italic" ).click(); 
		$( this ).toggleClass ('active');	
	});
	
	$( "#backbtnunderline" ).click(function() {
		$( "#backtext-cmd-underline" ).click();
		$( this ).toggleClass ('active');
		if ( $( this ).hasClass( "active" ) ) {		
			$( "#backbtnstrikethrough" ).removeClass ('active'); 
		}			
	});
	$( "#backbtnstrikethrough" ).click(function() {
		$( "#backtext-cmd-linethrough" ).click();
		$( this ).toggleClass ('active');
		if ( $( this ).hasClass( "active" ) ) {		
			$( "#backbtnunderline" ).removeClass ('active'); 
		}				
	});
        
      //Text fields
	  
	  document.getElementById('backtext-color').onchange = function() {
          backcanvas.getActiveObject().setFill(this.value);
          backcanvas.renderAll();
      };
      document.getElementById('backtext-color').onchange = function() {
          backcanvas.getActiveObject().setFill(this.value);
          backcanvas.renderAll();
      };
		
		document.getElementById('backtext-bg-color').onchange = function() {
          backcanvas.getActiveObject().setBackgroundColor(this.value);
          backcanvas.renderAll();
      };
		
		document.getElementById('backtext-lines-bg-color').onchange = function() {
          backcanvas.getActiveObject().setTextBackgroundColor(this.value);
          backcanvas.renderAll();
      };

		document.getElementById('backtext-stroke-color').onchange = function() {
          backcanvas.getActiveObject().setStroke(this.value);
          backcanvas.renderAll();
      };	

		document.getElementById('backtext-stroke-width').onchange = function() {
          backcanvas.getActiveObject().setStrokeWidth(this.value);
          backcanvas.renderAll();
      };				
	
		document.getElementById('backfont-family').onchange = function() {
          backcanvas.getActiveObject().setFontFamily(this.value);
          backcanvas.renderAll();
      };
		
		document.getElementById('backtext-font-size').onchange = function() {
          backcanvas.getActiveObject().setFontSize(this.value);
          backcanvas.renderAll();
      };
		
		document.getElementById('backtext-line-height').onchange = function() {
          backcanvas.getActiveObject().setLineHeight(this.value);
          backcanvas.renderAll();
      };
		
		document.getElementById('backtext-align').onchange = function() {
          backcanvas.getActiveObject().setTextAlign(this.value);
          backcanvas.renderAll();
      }; 
        
    }//endif - virgin  
    
  
	backcanvas.on('object:selected', onObjectSelected_back);
	backcanvas.on('selection:cleared', onObjectSelectedCleared_back);
		 
	  observeNumeric('padding');  
    
	radios5 = document.getElementsByName("backfonttype");  // wijzig naar button
  	for(var i = 0, max = radios5.length; i < max; i++) {
      radios5[i].onclick = function() {
          
          if(document.getElementById(this.id).checked == true) {
              if(this.id == "backtext-cmd-bold") {
                  backcanvas.getActiveObject().set("fontWeight", "bold");
              }
              if(this.id == "backtext-cmd-italic") {
                  backcanvas.getActiveObject().set("fontStyle", "italic");
              }
              if(this.id == "backtext-cmd-underline") {
                  backcanvas.getActiveObject().set("textDecoration", "underline");
              }
				if(this.id == "backtext-cmd-linethrough") {
                  backcanvas.getActiveObject().set("textDecoration", "line-through");
              }
				if(this.id == "backtext-cmd-overline") {
                  backcanvas.getActiveObject().set("textDecoration", "overline");
              }
              
              
              
          } else {
              if(this.id == "backtext-cmd-bold") {
                  backcanvas.getActiveObject().set("fontWeight", "");
              }
              if(this.id == "backtext-cmd-italic") {
                  backcanvas.getActiveObject().set("fontStyle", "");
              }  
              if(this.id == "backtext-cmd-underline") {
                  backcanvas.getActiveObject().set("textDecoration", "");
              }
				if(this.id == "backtext-cmd-linethrough") {
                  backcanvas.getActiveObject().set("textDecoration", "");
              }  
              if(this.id == "backtext-cmd-overline") {
                  backcanvas.getActiveObject().set("textDecoration", "");
              }
          }
          
          
          backcanvas.renderAll();
      }
    }//end for

}//end doinit function
	 

});

