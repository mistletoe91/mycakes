<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/homepage.css" rel="stylesheet">
 


<div class="container twocolone">
<div class="row"> 
<main class="bs-docs-masthead my-conta1iner" style="background-color:#FCFBFB" >  
<div class="row">
  <div class="col-md-6">
    <div class=""> 
 <div class="starter-template">
<h1 class="page-header"><span> Order Cakes Online !</span></h1>
<p class="lead">We made it simple to order and track <br> + 
         
<a class="no_underline_on_hover" data-toggle="tooltip" data-placement="right" title="Shipping is free in Toronto, Vaughan, Markham, Oakville, Mississauga, Caledon, Brampton, Burlington, Halton Hills, Milton, Richmond Hill and King"> 
                            FREE Delivery in 12 cities
                        </a>        </p> 

 
</div>
    </div>   
  </div>
  <div class="col-md-6"> 
      <img src="http://via.placeholder.com/350x150"  style="width:100%;margin-top:30px;"  /> 
  </div>
</div> 
</main>  
  </div>
  <!-- end row -->
  
<div class="row hide" style="padding-bottom:40px;background-color:#FCFBFB;">
  <div class="col-md-3"></div>
  <div class="col-md-3" style="text-align:right;"> <p><a href="/Flyers" class="btn btn-info btn-lg">See all Flyers</a></p></div>
  <div class="col-md-3" style="text-align:left;"> <p><a href="/Postcards" class="btn btn-info btn-lg">See all Postcards</a></p></div>
  <div class="col-md-3"></div>
</div> 
<!-- end row -->

</div>
<!-- end container -->



  
    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <h1>Cake Order Form</h1> 
          
      </div> 
   

      
<!-- Content Start ********************************************************************************************** -->
  <div class="row" style="margin-top:20px; ">
  	<div class="col-md-8">  
         
        
  <div class="panel-group accordionclass"  id="accordion1" role="tablist" aria-multiselectable="true">   
       
 
<div class="panel panel-default" id="divaccordion_1">
    <div class="panel-heading" role="tab" id="heading1_1">
      <h4 class="panel-title">
        <a id="collapse1_1_btn" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_1" aria-expanded="true" aria-controls="collapse1_1">
          Cake Order Form
        </a>
      </h4>
    </div>
    <div id="collapse1_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        
        <div class="row">  
            <div class="col-md-6">   
                  <p>Select Size</p>
                  <div class="mainselect_div"> 
                    <select id="ctlsize">  
                    </select>                       
                  </div>
                
                  <p>Select Type</p>
                  <div class="mainselect_div"> 
                    <select id="ctltype">  
                    </select>                       
                  </div>                

                  <p p>Select Color</p>
                  <div class="mainselect_div"> 
                    <select id="ctlcolor"> 
                    </select>                       
                  </div>        
                
                  <p>Select Decoration</p>
                  <div class="mainselect_div"> 
                    <select id="ctldecoration"> 
                    </select>                       
                  </div>         
                
                
            </div>
            <div class="col-md-6">   
              
                <div class="[ info-card ]">
					<img   src="/image/cake.svg" alt="Cake" />
					<div class="[ info-card-details ] animate">
						<div class="[ info-card-header ]">
							<h1> Order Your Cake  </h1>
							<h3> Free Delivery in GTA within 24-48 hrs  </h3>
						</div>
						<div class="[ info-card-detail ]">
							<!-- Description -->
							<p>The idea of creating something out of nothing has always generated a passion in my heart.  </p>
							<div class="social"> 
							</div>
						</div>
					</div>
				</div>                
                
                
                
            </div>            
        </div>       
         
        <div class="row">
            <div class="col-md-12 btnrow">    
                <a id="href_1_1" href="#" class="href_1 btn   btn-primary"><span class="glyphicon glyphicon-share-alt"></span> Next - Customize Cake</a>
            </div>
        </div>
           
          
                    
          <?php /*
        <div class="row overallbox" style=""> 
          <div class="col-md-4"> 
              <a id="href_1_1" class="href_1" href="#"><i  style="font-size: 80px;" class="fa fa-battery-quarter" aria-hidden="true"></i></a>
              <div class="hrefval"></div>
          </div>
          <div class="col-md-4"> 
              <a id="href_1_2" class="href_1" href="#"><i style="font-size: 80px;" class="fa fa-battery-half" aria-hidden="true"></i></a>
              <div class="hrefval"></div>
          </div>
          <div class="col-md-4"> 
              <a id="href_1_3" class="href_1" href="#"><i  style="font-size: 80px;" class="fa fa-battery-full" aria-hidden="true"></i> </a>
              <div class="hrefval"></div>
          </div> 
        </div>
        */ ?>
    
      </div>
    </div>
  </div>
      
      <?php /*
  <div class="panel panel-default" id="divaccordion_2">
    <div class="panel-heading" role="tab" id="heading1_2">
      <h4 class="panel-title">
        <a id="collapse1_2_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_2" aria-expanded="false" aria-controls="collapse1_2">
         Choose Cake Type
        </a>
      </h4>
    </div>
    <div id="collapse1_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
          
        <div class="row overallbox" style=""> 
          <div class="col-md-6"> 
              <a id="href_2_1" class="href_2" href="#"><i  style="font-size: 80px;" class="fa fa-battery-quarter" aria-hidden="true"></i></a>
              <div class="hrefval">Chocolate Cake</div>
          </div>
          <div class="col-md-6"> 
              <a id="href_2_1" class="href_2" href="#"><i style="font-size: 80px;" class="fa fa-battery-half" aria-hidden="true"></i></a>
              <div class="hrefval">White Cake</div>
          </div> 
        </div> 
          
      </div>
    </div>
  </div>
  */ ?>
  <div class="panel panel-default" id="divaccordion_2">
    <div class="panel-heading" role="tab" id="heading1_2">
      <h4 class="panel-title">
        <a id="collapse1_2_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_2" aria-expanded="false" aria-controls="collapse1_2">
          Choose Cake Filing
        </a>
      </h4>
    </div>
    <div id="collapse1_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
 
        <div class="row">  
            <div class="col-md-4">  
                <div class="funkyradio clsfilling1">  
                     
                </div>
            </div>
            <div class="col-md-4">  
                <div class="funkyradio clsfilling2">  

                   
                </div>
            </div>
            <div class="col-md-4">  
                <div class="funkyradio clsfilling3">    
                </div>
            </div>            
        </div>   
        <!-- End Row -->
          
      </div>
    </div>
  </div>      
  <div class="panel panel-default" id="divaccordion_3">
    <div class="panel-heading" role="tab" id="heading1_3">
      <h4 class="panel-title">
        <a id="collapse1_3_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_3" aria-expanded="false" aria-controls="collapse1_3">
          Choose Cake Icining
        </a>
      </h4>
    </div>
    <div id="collapse1_3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <div class="row">
            <div class="col-md-4">  
                <div class="funkyradio clsicing1"> 
                </div>
            </div>
 
            <div class="col-md-4">  
                <div class="funkyradio clsicing2">   
                </div>
            </div>
            <div class="col-md-4 clsicing3">  
                <div class="funkyradio">  
                </div>
            </div>            
         </div>
        <!-- End Row -->
          
      </div>
    </div>
  </div>
   
      <?php /*
  <div class="panel panel-default" id="divaccordion_5">
    <div class="panel-heading" role="tab" id="heading1_5">
      <h4 class="panel-title">
        <a id="collapse1_5_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_5" aria-expanded="false" aria-controls="collapse1_5">
          Trim Colour
        </a>
      </h4>
    </div>
    <div id="collapse1_5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1_5">
      <div class="panel-body">
        <div class="row">
           <div class="col-md-4">  
                <div class="funkyradio">  
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio311" checked/><label for="radio311">White</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio312"  /><label for="radio312">Brown</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio313"  /><label for="radio313">Burgundy</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio314"  /><label for="radio314">Green</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio315"  /><label for="radio315">Holiday Red</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio316"  /><label for="radio316">Lavender</label></div>
                </div>
           </div>
           <div class="col-md-4">  
                <div class="funkyradio">   
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio317"  /><label for="radio317">Mauve</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio318"  /><label for="radio318">Orange</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio319"  /><label for="radio319">Pink (Fuschia)</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio3111"  /><label for="radio3111">Purple</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio3112"  /><label for="radio3112">Red</label></div>
                </div>
           </div>
           <div class="col-md-4">  
                <div class="funkyradio">                                      
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio3113"  /><label for="radio3113">Royal Blue</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio3114"  /><label for="radio3114">Sky Blue</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio3115"  /><label for="radio3115">Violet</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio3116"  /><label for="radio3116">Yellow</label></div> 
                </div>
            </div>   
        </div>
        <!-- End Row -->             
          
      </div>
    </div>
  </div>
 
      
  <div class="panel panel-default" id="divaccordion_6">
    <div class="panel-heading" role="tab" id="heading1_6">
      <h4 class="panel-title">
        <a id="collapse1_6_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_6" aria-expanded="false" aria-controls="collapse1_6">
          Choose Decoration
        </a>
      </h4>
    </div>
    <div id="collapse1_6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1_6">
      <div class="panel-body">
        <div class="row">
            <div class="col-md-4">  
                <div class="funkyradio">  
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio70" checked/><label for="radio70">None</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio71"  /><label for="radio71">One Happy Birthday Sign</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio72"  /><label for="radio72">Two Clown Heads</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio77"  /><label for="radio77">Red Ribbon</label></div>
                </div>
            </div>
            <div class="col-md-4">  
                <div class="funkyradio">  
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio73"  /><label for="radio73">One Balloon Cluster</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio74"  /><label for="radio74">Foil Presents</label></div>

                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio79"  /><label for="radio79">Blue Ribbon</label></div>
                </div>
            </div>
            <div class="col-md-4">  
                <div class="funkyradio">  
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio75"  /><label for="radio75">Silver Ribbon</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio76"  /><label for="radio76">Lavender Ribbon</label></div>                     
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio78"  /><label for="radio78">Yellow Ribbon</label></div>
                </div>
            </div>            
         </div>
        <!-- End Row -->      
          
      </div>
    </div>
  </div>   
   */ ?>
      
  <div class="panel panel-default" id="divaccordion_7">
    <div class="panel-heading" role="tab" id="heading1_7">
      <h4 class="panel-title">
        <a id="collapse1_7_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_7" aria-expanded="false" aria-controls="collapse1_7">
          Write Message On Cake
        </a>
      </h4>
    </div>
    <div id="collapse1_7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1_7">
      <div class="panel-body">
        <div class="row">&nbsp;</div>
        <div class="row">
            <div class="col-md-6">   
                  <div class="input-group">      
                    <input  type="text"  required >
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label  class="fancytextinputlabels">Message Line 1</label>
                  </div>
                    
                  <div class="input-group">      
                    <input   type="text"  required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Message Line 2</label>
                  </div> 
                 
                  <div class="input-group">      
                    <input   type="text"  required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Message Line 3</label>
                  </div>              
            </div>   
            
            <div class="col-md-6">   
              
                <div class="[ info-card ]">
					<img   src="/image/cake.svg" alt="Cake" />
					<div class="[ info-card-details ] animate">
						<div class="[ info-card-header ]">
							<h1> Write custom Message  </h1>
							<h3> upto 3 lines 25 char each </h3>
						</div>
						<div class="[ info-card-detail ]">
							 Description 
							<p>The idea of creating something out of nothing has always generated a passion in my heart.  </p>
							<div class="social"> 
							</div>
						</div>
					</div>
				</div>                
                
                
                
            </div>

            
         </div>
        <!-- End Row -->
          
        <div class="row">
            <div class="col-md-12 btnrow">     
                <a id="href_delivery_details" href="#" class="btn  btn-primary"><span class="glyphicon glyphicon-share-alt"></span> Next - Delivery Details</a>
            </div>
        </div>          
          
      </div>
    </div>
  </div>       
       
  <div class="panel panel-default" id="divaccordion_8">
    <div class="panel-heading" role="tab" id="heading1_8">
      <h4 class="panel-title">
        <a id="collapse1_8_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_8" aria-expanded="false" aria-controls="collapse1_8">
          Delivery Details
        </a>
      </h4>
    </div>
    <div id="collapse1_8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1_8">
      <div class="panel-body">
        <div class="row">&nbsp;</div>
        <div class="row">
            <div class="col-md-6">    
                  <div class="input-group">      
                    <input id="autocomplete" placeholder="Start Typing Your Address" onFocus="geolocate()" type="text"></input>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label  class="fancytextinputlabels">Delivery Address</label>
                  </div>
                    
                
 <table id="address" class="hide">
      <tr>
        <td class="label">Street address</td>
        <td class="slimField"><input class="field" id="street_number"
              disabled="true"></input></td>
        <td class="wideField" colspan="2"><input class="field" id="route"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">City</td>
        <!-- Note: Selection of address components in this example is typical.
             You may need to adjust it for the locations relevant to your app. See
             https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete-addressform
        -->
        <td class="wideField" colspan="3"><input class="field" id="locality"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">Postal</td>
        <td class="slimField"><input class="field"
              id="postal_code" disabled="true"></input></td>
    
      </tr>        
      <tr>
        <td class="label">State</td>
        <td class="slimField"><input class="field"
              id="administrative_area_level_1" disabled="true"></input></td>
    
      </tr>
      <tr>
        <td class="label">Country</td>
        <td class="wideField" colspan="3"><input class="field"
              id="country" disabled="true"></input></td>
      </tr>
    </table>  
                  <div class="input-group">      
                    <input   type="text"  required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Delivery Instructions e.g. Buzzer#</label>
                  </div> 
                                 
                 
            </div>   

            <div class="col-md-6">   
              
                <div class="[ info-card ]">
					<img   src="/image/map.png" alt="Security Seal" />
					<div class="[ info-card-details ] animate">
						<div class="[ info-card-header ]">
							<h1> Always FREE Delivery  </h1>
							<h3> We make sure to deliver fast </h3>
						</div>
						<div class="[ info-card-detail ]">
							<!-- Description -->
							<p>The idea of creating something out of nothing has always generated a passion in my heart.  </p>
							<div class="social"> 
							</div>
						</div>
					</div>
				</div>                
                
                
                
            </div>

         </div>
        <!-- End Row -->       

        <div class="row">
            <div class="col-md-12 btnrow">     
                <button id="href_payment_details" disabled  type="button" class="btn btn-primary"><span class="glyphicon glyphicon-share-alt"></span> Final Step - Payment Information</button>
                <div id="div_deliverymsg"></div>
            </div>
        </div>
          
      </div>
    </div>
  </div>         
<!-- end Panel -->      
      
   
        
  <div class="panel panel-default" id="divaccordion_9">
    <div class="panel-heading" role="tab" id="heading1_9">
      <h4 class="panel-title">
        <a id="collapse1_9_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_9" aria-expanded="false" aria-controls="collapse1_9">
          Payment Details
        </a>
      </h4>
    </div>
    <div id="collapse1_9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1_9">
      <div class="panel-body">
        <div class="row">&nbsp;</div>
        <div class="row">
            <div class="col-md-6">   
                
<form role="form" id="payment-form" method="POST" action="javascript:void(0);">
  
    
                  <div class="input-group">      
                      <input 
                                            type="tel"
                                            class="payment-field"
                                            name="cardNumber" 
                                            required autofocus 
                                        />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Credit Card#</label>
                  </div>   
    
                  <div class="input-group">      
                     <input name="cardExpiry" type="tel" class="payment-field"   required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Expiry (mm/yyyy)</label>
                  </div>       
    
                  <div class="input-group">      
                   <input name="cardCVC" type="tel" class="payment-field"   required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">CVC</label>
                  </div>     
    <?php /*
                  <div class="input-group">      
                    <input   type="text"   name="cardholder-name"  required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Billing Name</label>
                  </div>
    
                  <div class="input-group">      
                    <input   type="text"   name="telephone_payment"  required>
                    <input   type="tel"   style="display:none;" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Phone</label>
                  </div>     
    
                  <div class="input-group">      
                    <input   type="text" name="address-zip"  required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Billing Postal Code</label>
                  </div>   
    */ ?>
     
    
  <?php /*  
                 <div class="input-group"  style="border-bottom:1px solid #00BFFF;">       
                    <div id="card-element" class="field"></div>  
                    <span class="highlight">Card#</span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels"></label>
                  </div> 
                  
  <label>
    <span>Name</span>
    <input name="cardholder-name" class="field" placeholder="Jane Doe" />
  </label>
  <label>
    <span>Phone</span>
    <input class="field" placeholder="(123) 456-7890" type="tel" />
  </label>
  <label>
    <span>ZIP code</span>
    <input name="address-zip" class="field" placeholder="94110" />
  </label>
  <label>
    <span>Card</span>
    <div id="card-element" class="field"></div>
  </label>
  */ ?>
    
                        <div class="row">
                            <div class="col-xs-12">
                                
                                <table class="paymenttable table-bordered table-condensed"> 
                                    <tr ><td class="info"><h4>Final Price</h4></td></tr>
                                  <tr> 
                                      <td id="payment_finalprice">Loading....</td>
                                  </tr>  
                                    <tr><td  style="font-size:13px;">Delivery is Free and estimated time <br />is between 24-48 hours</td></tr>
                                </table>
                                
                            </div>
                        </div>    
    
                        <div class="row">
                            <div class="col-xs-12">
                                <button id="payment_btn" class="subscribe btn btn-success btn-lg btn-block" type="button">Loading....</button>
                            </div>
                        </div>
                        <div class="row" style="display:none;">
                            <div class="col-xs-12">
                                <p class="payment-errors"></p>
                            </div>
                        </div>  
 
</form>
                
            </div>     
            <div class="col-md-6">   
              
                <div class="[ info-card ]">
					<img   src="/image/security_seal.png" alt="Security Seal" />
					<div class="[ info-card-details ] animate">
						<div class="[ info-card-header ]">
							<h1> Seucure Website  </h1>
							<h3> We do not secure your CC info </h3>
						</div>
						<div class="[ info-card-detail ]">
							<!-- Description -->
							<p>The idea of creating something out of nothing has always generated a passion in my heart.  </p>
							<div class="social"> 
							</div>
						</div>
					</div>
				</div>                
                
                
                
            </div>
         </div>
        <!-- End Row -->              
          
      </div>
    </div>
  </div>  

        


        
  


  


 
        </div> <!-- end of main accordian --> 
  	</div><!-- END col-md-8 -->
  <div class="col-md-4">
  	
  	
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"> Final Price</h3> 
				</div>
                <div class="panel-collapse collapse in">
				    <div class="panel-body" style="text-align:center;">
                        <h1 class="placeholder_finalprice">$0<small>+ HST</small></h1>
                        
                                                  
                                
                        <a class="no_underline_on_hover" data-toggle="tooltip" data-placement="right" title="Shipping is free in Toronto, Brampton, Mississauga, Caledon, Oakville, Markham, Milton, Scarborough, Vaughan, Brampton etc."> 
                            FREE Shipping
                        </a>
                    </div>
                </div>
			</div>
      
      <?php /*
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingZero">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Final Price
          

                      
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingZero">
      <div class="panel-body" style="text-align:center;"> 
        
 	        <h1 class="placeholder_finalprice">$123.31<small>+ HST</small></h1> 
         
<div style="display:none">         
<div style="display: inline-block; "> 
  <p>
    <label>
      <code>padding</code>
      <input id="padding" value="0" min="0" max="50" style="display: block" type="range">
    </label>
  </p> 
</div>      
</div>
         
      </div>
    </div>
  </div> 
       
      
     
 
  <div class="panel panel-default hide">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Nothing here
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body"> 
      </div>
    </div>
  </div>  
</div>  	
  	 */ ?>
    
    
  	</div>
  </div>
 
      
 <!-- Portfolio -->
<div class="row" >
  <div class="starter-template">
        <h1 class="page-header">Why Chose Our   <span>Cakes ?</span></h1>
        <p class="lead">You will get all of the following regardless. So why don`t you sign up today ? Just do it </p>
  </div>

    <div class="row stylish-panel">
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-cloud  insideicons"  ></i>
             </div>
           </div>
           <h2>While-label Service</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div>
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-television  insideicons" ></i>
             </div>
           </div>
           <h2>Easy-Embedd Widget</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div>
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-mobile-phone insideicons" ></i>
             </div>
           </div>
           <h2>Mobile App</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div>
    </div>

    <div class="row stylish-panel">
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-shield insideicons"  ></i>
             </div>
           </div>
           <h2>Robust Security</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management .
           </p>
        </div>
      </div>
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-hand-o-up insideicons" ></i>
             </div>
           </div>
           <h2>Control Document Transfer</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div> 
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-thumb-tack insideicons" ></i>
             </div>
           </div>
           <h2>Tracking</h2>
           <p class="moreinfotext">TrThis service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div>
    </div>
    <div style="Clear:both"></div>
</div>
<div style="Clear:both"></div>
<!-- /Portfolio -->



            </div>





 

<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>