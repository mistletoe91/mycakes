<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  
  <!-- Canvas 1 -->
  <script src="catalog/view/javascript/fabric.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/product.js" type="text/javascript"></script> 
  <script src="catalog/view/javascript/product_back.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/multistepform.js" type="text/javascript"></script>
  
  
  <!-- Multi Step Form Start -->


    <div class="">
	    <div class="row form-group">
            <div class="col-xs-12">
                <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                    <li class="active"><a href="#step-1">
                        <h4 class="list-group-item-heading">Step 1</h4>
                        <p class="list-group-item-text">Front Side</p>
                    </a></li>
                    <li class="disabled"><a href="#step-2">
                        <h4 class="list-group-item-heading">Step 2</h4>
                        <p class="list-group-item-text">Back Side</p>
                    </a></li>
                    <li class="disabled"><a href="#step-3" class="clsgetfinalproof">
                        <h4 class="list-group-item-heading">Step 3</h4>
                        <p class="list-group-item-text">See Proofs & Finalize</p>
                    </a></li>    
                </ul>
            </div>
	    </div>
    </div>	

<div class="">

    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12   text-center">
                <h1> STEP 1</h1>
                <h3>Choose Front Side - draw something , make some stuff </h3>
                <hr />

   <div class="row">
  	<div class="col-md-12" style="text-align:left;"> 
  	       <input type="file" style="display:none;" id="btnfileupload">
  	       
		     	       
  	       <button data-toggle="tooltip" data-placement="top" title="Upload Image" class="btn" id="fileuploadbtn"><i class="fa fa-upload" aria-hidden="true"></i></button>
  	       <button data-toggle="tooltip" data-placement="top" title="Add Text" class="btn" id="addnewtext"><i class="fa fa-font" aria-hidden="true"></i></button>
      	   <button data-toggle="tooltip" data-placement="top" title="Bring Forward" class="btn" id="sendSelectedObjectToFront" ><i class="fa fa-chevron-up" aria-hidden="true"></i></button>
           <button data-toggle="tooltip" data-placement="top" title="Send To Back" class="btn" id="sendSelectedObjectBack" ><i class="fa fa-chevron-down" aria-hidden="true"></i></button>  	
           <button data-toggle="tooltip" data-placement="top" title="Remove" class="btn btn-danger" id="deletebtn"><i class="fa fa-trash" aria-hidden="true"></i></button>       
  	</div>
  </div>  
  
  <div class="row" style="margin-top:20px;">
  	<div class="col-md-8">  
  	     <canvas id="canvas-product-front"></canvas> 
  	     <div id="overlaycanvas" STYLE="position:absolute; top:20px; left:30px;"></div>
  	     <div id="placeholdercanvas" style="display:none;"></div>
  	</div>
  	<div class="col-md-4">
  	
  	
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingZero">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Select Options
          

                      
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingZero">
      <div class="panel-body"> 
       
            <?php   
                 $initialPriceToShow = '$';
                 foreach ($discounts as $discount) { 
                    if($minimum ===  $discount['quantity']){  

                      $qty = preg_replace("/[^0-9\.]/", '', $discount['quantity']);
                      $price = preg_replace("/[^0-9\.]/", '', $discount['price']);

                      $initialPriceToShow .= +number_format((float)$qty * $price, 2, '.', ''); 
                      break;
                    }#endif
                 }#end foreach 
            ?> 
 	        <h1 class="placeholder_finalprice"><?php echo $initialPriceToShow;?></h1> 
 	        <select  style="margin-bottom:20px;" class="placeholder_discount form-control">        
            <?php foreach ($discounts as $discount) { ?>
            <option value="<?php echo $discount['quantity'].'_'.$discount['price']; ?>"><?php echo $discount['price']; ?> - <?php echo number_format($discount['quantity'], 0); ?> pcs</option>
            <?php } ?>
            </select>  
            <button class="btn btn-primary" id="savebtn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save and Go to Step 2</button>       
         
<div style="display:none">         
<div style="display: inline-block; "> 
  <p>
    <label>
      <code>padding</code>
      <input id="padding" value="0" min="0" max="50" style="display: block" type="range">
    </label>
  </p> 
</div>      
</div>
         
      </div>
    </div>
  </div> 
  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Related Templates
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
      

      
      
  <div id="text-wrapper" style="margin-top: 10px;border:1px solid #333;padding:5px;background-color:#ddd;"  > 
    <div id="text-controls">

       


      
      
<div class="row">
  <div class="col-md-6">
      <select id="font-family" class="form-control">
        <option value="arial" selected>Arial</option>
        <option value="helvetica" >Helvetica</option>
        <option value="myriad pro">Myriad Pro</option>
        <option value="delicious">Delicious</option>
        <option value="verdana">Verdana</option>
        <option value="georgia">Georgia</option>
        <option value="courier">Courier</option>
        <option value="comic sans ms">Comic Sans MS</option>
        <option value="impact">Impact</option>
        <option value="monaco">Monaco</option>
        <option value="optima">Optima</option>
        <option value="hoefler text">Hoefler Text</option>
        <option value="plaster">Plaster</option>
        <option value="engagement">Engagement</option>
        <option value="times new roman">Times New Roman</option> 
      </select>
   </div>
   <div class="col-md-6">
      <select id="text-font-size" class="form-control">
        <?php
          for($i=1;$i<=120;$i++){
               echo '<option value="'.$i.'">'.$i.'</option>';
          } 
        ?> 
      </select> 
    </div> 
  </div>
      
     
       

  
        <div class="btn-group" role="group"  >
  <button type="button" id="btnleftalign"  class="btn btn-default"><i class="fa fa-align-left" aria-hidden="true"></i></button>
  <button type="button" id="btncentertalign" class="btn btn-default"><i class="fa fa-align-center" aria-hidden="true"></i></button>
  <button type="button"  id="btnrightalign" class="btn btn-default"><i class="fa fa-align-right" aria-hidden="true"></i></button> 
</div>
      
<div class="btn-group" role="group"  >
  <button type="button" id="btnbold"  class="btn btn-default"><i class="fa fa-bold" aria-hidden="true"></i></button>
  <button type="button" id="btnitalic"   class="btn btn-default"><i class="fa fa-italic" aria-hidden="true"></i></button>
</div>  
<div class="btn-group" role="group"  >  
  <button type="button" id="btnunderline"   class="btn btn-default"><i class="fa fa-underline" aria-hidden="true"></i></button>
  <button type="button" id="btnstrikethrough"   class="btn btn-default"><i class="fa fa-strikethrough" aria-hidden="true"></i></button> 
</div>

      
<div class="btn-group" role="group"  >
  <div class="btn-group" role="group">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Select Colors
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
      <li><input type="color" value="" id="text-color" size="10"> Text</li>
      <li><input type="color" value="" id="text-bg-color" size="10"> Background</li>
    </ul>
  </div>
</div>       
      
</div>       
      
      <div style="display:none;">
      <select id="text-align">
        <option value="left">Left</option>
        <option value="center">Center</option>
        <option value="right">Right</option>
        <option value="justify">Justify</option>
      </select>       
       
      <li><input type="color" value="" style="display:none;" id="text-lines-bg-color" size="10"> Background Text Color</li>
      <li><input type="color" value="" style="display:none;" id="text-stroke-color"> Stroke Color</li> 
      
       <input type="range" value="1" style="display:none;" min="1" max="5" id="text-stroke-width">
       <input type="range" value="1" style="display:none;" min="0" max="10" step="0.1" id="text-line-height">
       </div>
             
      <?php /*
       <input type="color" value="" id="text-color" size="10">
      <div>
        <label for="text-bg-color">Background color:</label>
        <input type="color" value="" id="text-bg-color" size="10">
      </div>
      <div>
        <label for="text-lines-bg-color">Background text color:</label>
        <input type="color" value="" id="text-lines-bg-color" size="10">
      </div>
      <div>
        <label for="text-stroke-color">Stroke color:</label>
        <input type="color" value="" id="text-stroke-color">
      </div>
      <div> 
        <label for="text-stroke-width">Stroke width:</label>
        <input type="range" value="1" min="1" max="5" id="text-stroke-width">
      </div>
      <div>
        <label for="text-font-size">Font size:</label> 
        <input type="range" value="" min="1" max="120" step="1" id="text-font-size"> 
      </div>
      <div>
        <label for="text-line-height">Line height:</label>
        <input type="range" value="" min="0" max="10" step="0.1" id="text-line-height">
      </div>
      */ 
      ?>
      
      
      
    </div>
    <div id="text-controls-additional" style="display:none;">
      <input type='checkbox' name='fonttype' id="text-cmd-bold">
        Bold
    
      <input type='checkbox' name='fonttype' id="text-cmd-italic">
        Italic
     
      <input type='checkbox' name='fonttype' id="text-cmd-underline" >
        Underline
      
      <input type='checkbox' name='fonttype'  id="text-cmd-linethrough">
        Linethrough
     
      <input type='checkbox' name='fonttype'  id="text-cmd-overline" >
        Overline 
    </div>   
       
      </div>
    </div>
  </div> 
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Some more useless tabs
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Don`t open this
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Never say never
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>  
</div>  	
  	 
  	</div>
  </div>
 
                 
            </div>
        </div>
    </div>

</div>

<!-- Step2 --> 
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12   text-center">
                <h1> STEP 2</h1>
                <h3>Choose Back Side - leave it blank or add </h3>
                <hr />

   <div class="row">
  	<div class="col-md-12" style="text-align:left;"> 
  	       <input type="file" style="display:none;" id="backbtnfileupload">
  	       
		     	       
  	       <button data-toggle="tooltip" data-placement="top" title="Upload Image" class="btn" id="backfileuploadbtn"><i class="fa fa-upload" aria-hidden="true"></i></button>
  	       <button data-toggle="tooltip" data-placement="top" title="Add Text" class="btn" id="backaddnewtext"><i class="fa fa-font" aria-hidden="true"></i></button>
      	   <button data-toggle="tooltip" data-placement="top" title="Bring Forward" class="btn" id="backsendSelectedObjectToFront" ><i class="fa fa-chevron-up" aria-hidden="true"></i></button>
           <button data-toggle="tooltip" data-placement="top" title="Send To Back" class="btn" id="backsendSelectedObjectBack" ><i class="fa fa-chevron-down" aria-hidden="true"></i></button>  	
           <button data-toggle="tooltip" data-placement="top" title="Remove" class="btn btn-danger" id="backdeletebtn"><i class="fa fa-trash" aria-hidden="true"></i></button>       
  	</div>
  </div>  
  
  <div class="row" style="margin-top:20px;">
  	<div class="col-md-8">  
  	     <DIV id="div_backcanvas-product-front">
              
         </DIV>
  	     <div id="backoverlaycanvas" STYLE="position:absolute; top:20px; left:30px;"></div>
  	     <div id="backplaceholdercanvas" style="display:none;"></div>
  	</div>
  	<div class="col-md-4">
  	 
<div class="panel-group" id="backaccordion" role="tablist" aria-multiselectable="true">

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="backheadingZero">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Select Options
        </a>
      </h4>
    </div>
    <div id="backcollapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingZero">
      <div class="panel-body">
      
            <?php 
                $price_to_show_at_entry = $price;
                foreach ($discounts as $discount) {
                    if($minimum ===  $discount['quantity']){
                           $price_to_show_at_entry = $discount['price'];
                    }#endif
                }#endforeach
            ?>   
 	        <h1 class="placeholder_finalprice"><?php echo $initialPriceToShow;?></h1> 
 	        <select  style="margin-bottom:20px;" class="placeholder_discount form-control">        
            <?php foreach ($discounts as $discount) { ?>
            <option value="<?php echo $discount['quantity'].'_'.$discount['price']; ?>"><?php echo $discount['price']; ?> - <?php echo number_format($discount['quantity'], 0); ?> pcs</option>
            <?php } ?>
            </select>     
            <button class="btn btn-primary" id="backsavebtn" class="clsbacksavebtn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save and Preview</button>      
          
<hr />
<div class="backsidetemplates">
 <H4>Or choose from following designs</H4>
 <div class="row"></div>
</div>          
         
<div style="display:none">         
<div style="display: inline-block; "> 
  <p>
    <label>
      <code>padding</code>
      <input id="backpadding" value="0" min="0" max="50" style="display: block" type="range">
    </label>
  </p> 
</div> 




</div>
         
      </div>
    </div>
  </div> 
  
  <div class="panel panel-default" style="display:none;">
    <div class="panel-heading" role="tab" id="backheadingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Related Templates
        </a>
      </h4>
    </div>
    <div id="backcollapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
      

      
      
  <div id="backtext-wrapper" style="margin-top: 10px;border:1px solid #333;padding:5px;background-color:#ddd;"  > 
    <div id="backtext-controls">

       


      
      
<div class="row">
  <div class="col-md-6">
      <select id="backfont-family" class="form-control">
        <option value="arial" selected>Arial</option>
        <option value="helvetica" >Helvetica</option>
        <option value="myriad pro">Myriad Pro</option>
        <option value="delicious">Delicious</option>
        <option value="verdana">Verdana</option>
        <option value="georgia">Georgia</option>
        <option value="courier">Courier</option>
        <option value="comic sans ms">Comic Sans MS</option>
        <option value="impact">Impact</option>
        <option value="monaco">Monaco</option>
        <option value="optima">Optima</option>
        <option value="hoefler text">Hoefler Text</option>
        <option value="plaster">Plaster</option>
        <option value="engagement">Engagement</option>
        <option value="times new roman">Times New Roman</option> 
      </select>
   </div>
   <div class="col-md-6">
      <select id="backtext-font-size" class="form-control">
        <?php
          for($i=1;$i<=120;$i++){
               echo '<option value="'.$i.'">'.$i.'</option>';
          } 
        ?> 
      </select> 
    </div> 
  </div>
      
     
       

  
        <div class="btn-group" role="group"  >
  <button type="button" id="backbtnleftalign"  class="btn btn-default"><i class="fa fa-align-left" aria-hidden="true"></i></button>
  <button type="button" id="backbtncentertalign" class="btn btn-default"><i class="fa fa-align-center" aria-hidden="true"></i></button>
  <button type="button"  id="backbtnrightalign" class="btn btn-default"><i class="fa fa-align-right" aria-hidden="true"></i></button> 
</div>
      
<div class="btn-group" role="group"  >
  <button type="button" id="backbtnbold"  class="btn btn-default"><i class="fa fa-bold" aria-hidden="true"></i></button>
  <button type="button" id="backbtnitalic"   class="btn btn-default"><i class="fa fa-italic" aria-hidden="true"></i></button>
</div>  
<div class="btn-group" role="group"  >  
  <button type="button" id="backbtnunderline"   class="btn btn-default"><i class="fa fa-underline" aria-hidden="true"></i></button>
  <button type="button" id="backbtnstrikethrough"   class="btn btn-default"><i class="fa fa-strikethrough" aria-hidden="true"></i></button> 
</div>

      
<div class="btn-group" role="group"  >
  <div class="btn-group" role="group">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Select Colors
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
      <li><input type="color" value="" id="backtext-color" size="10"> Text</li>
      <li><input type="color" value="" id="backtext-bg-color" size="10"> Background</li>
    </ul>
  </div>
</div>       
      
</div>       
      
      <div style="display:none;">
      <select id="backtext-align">
        <option value="left">Left</option>
        <option value="center">Center</option>
        <option value="right">Right</option>
        <option value="justify">Justify</option>
      </select>       
       
      <li><input type="color" value="" style="display:none;" id="backtext-lines-bg-color" size="10"> Background Text Color</li>
      <li><input type="color" value="" style="display:none;" id="backtext-stroke-color"> Stroke Color</li> 
      
       <input type="range" value="1" style="display:none;" min="1" max="5" id="backtext-stroke-width">
       <input type="range" value="1" style="display:none;" min="0" max="10" step="0.1" id="backtext-line-height">
       </div>
             
      <?php /*
       <input type="color" value="" id="backtext-color" size="10">
      <div>
        <label for="text-bg-color">Background color:</label>
        <input type="color" value="" id="backtext-bg-color" size="10">
      </div>
      <div>
        <label for="text-lines-bg-color">Background text color:</label>
        <input type="color" value="" id="backtext-lines-bg-color" size="10">
      </div>
      <div>
        <label for="text-stroke-color">Stroke color:</label>
        <input type="color" value="" id="backtext-stroke-color">
      </div>
      <div> 
        <label for="text-stroke-width">Stroke width:</label>
        <input type="range" value="1" min="1" max="5" id="backtext-stroke-width">
      </div>
      <div>
        <label for="text-font-size">Font size:</label> 
        <input type="range" value="" min="1" max="120" step="1" id="backtext-font-size"> 
      </div>
      <div>
        <label for="text-line-height">Line height:</label>
        <input type="range" value="" min="0" max="10" step="0.1" id="backtext-line-height">
      </div>
      */ 
      ?>
      
      
      
    </div>
    <div id="backtext-controls-additional" style="display:none;">
      <input type='checkbox' name='backfonttype' id="backtext-cmd-bold">
        Bold
    
      <input type='checkbox' name='backfonttype' id="backtext-cmd-italic">
        Italic
     
      <input type='checkbox' name='backfonttype' id="backtext-cmd-underline" >
        Underline
      
      <input type='checkbox' name='backfonttype'  id="backtext-cmd-linethrough">
        Linethrough
     
      <input type='checkbox' name='backfonttype'  id="backtext-cmd-overline" >
        Overline 
    </div>   
       
      </div>
    </div>
  </div> 
</div>  	
  	 
  	</div>
  </div>
 
                 
            </div>
        </div>
    </div>
 

<div class="container">

    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12 well text-center">
                <h1 class="text-center"> STEP 2</h1>
                
 
                
                <button id="activate-step-3" class="btn btn-primary btn-md">Activate Step 3</button>
            </div>
        </div>
    </div>

</div>

<div class="">

    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12  text-center"> 
                
                <h1> STEP 3</h1>
                <h3>Check Proof Below</h3>
                <hr />
                
                <div class="row">
                    <div class="col-md-8">
                
                
                <div id="clsproof_front" style=""><img src="/image/l.gif" alt="Loading..." /></div>
                <h3>Front Side</h3> 
                 
                <div id="clsproof_back" style=""><img src="/image/l.gif" alt="Loading..." /></div>
                <h3>Back Side</h3>                     
                    </div>
                    <div class="col-md-4">
                    
                    
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Select Option
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
            
            <?php 
                $price_to_show_at_entry = $price;
                foreach ($discounts as $discount) {
                    if($minimum ===  $discount['quantity']){
                           $price_to_show_at_entry = $discount['price'];
                    }#endif
                }#endforeach
            ?>   
 	        <h1 class="placeholder_finalprice"><?php echo $initialPriceToShow;?></h1> 
 	        <select style="margin-bottom:20px;" class="placeholder_discount form-control">        
            <?php foreach ($discounts as $discount) { ?>
            <option value="<?php echo $discount['quantity'].'_'.$discount['price']; ?>"><?php echo $discount['price']; ?> - <?php echo number_format($discount['quantity'], 0); ?> pcs</option>
            <?php } ?>
            </select>  
               
            <p>By clicking following button you agree that the final product will be printed as the proof shown and <a>all other conditions listed here</a> and there are whatsoever no futher things ! Final Sale Forever !</p>   
            <button  type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>      
      </div>
    </div>
  </div> 
</div>
                     
 
                  
                 
  <!-- Panel End -->
              
                    </div>
                </div>

                 
            </div>
        </div>
    </div>

</div>

<div class="container">
    
    <div class="row setup-content" id="step-4">
        <div class="col-xs-12">
            <div class="col-md-12 well text-center">
                <h1 class="text-center"> STEP 4</h1>
                
              
                
            </div>
        </div>
    </div>

</div>

    
  
  <!-- Multi Step Form End -->  
  
  <div class="row" style="display:none" id="divproof">
  	<div class="col-md-12"> 
  	       Processing...       
  	</div>
  </div>     
   


  <div class="row hide "><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row">
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-8'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
          <?php if ($thumb || $images) { ?>
          <ul class="thumbnails">
            <?php if ($thumb) { ?>
            <li><a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
            <?php } ?>
            <?php if ($images) { ?>
            <?php foreach ($images as $image) { ?>
            <li class="image-additional"><a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php 
                   
          
          ?>
          <?php } ?>
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
            <?php if ($attribute_groups) { ?>
            <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
            <?php } ?>
            <?php if ($review_status) { ?>
            <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
            <?php } ?>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-description"><?php echo $description; ?></div>
            <?php if ($attribute_groups) { ?>
            <div class="tab-pane" id="tab-specification">
              <table class="table table-bordered">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                  <tr>
                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                  <tr>
                    <td><?php echo $attribute['name']; ?></td>
                    <td><?php echo $attribute['text']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php } ?>
              </table>
            </div>
            <?php } ?>
            <?php if ($review_status) { ?>
            <div class="tab-pane" id="tab-review">
              <form class="form-horizontal" id="form-review">
                <div id="review"></div>
                <h2><?php echo $text_write; ?></h2>
                <?php if ($review_guest) { ?>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                    <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                    <div class="help-block"><?php echo $text_note; ?></div>
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label"><?php echo $entry_rating; ?></label>
                    &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                    <input type="radio" name="rating" value="1" />
                    &nbsp;
                    <input type="radio" name="rating" value="2" />
                    &nbsp;
                    <input type="radio" name="rating" value="3" />
                    &nbsp;
                    <input type="radio" name="rating" value="4" />
                    &nbsp;
                    <input type="radio" name="rating" value="5" />
                    &nbsp;<?php echo $entry_good; ?></div>
                </div>
                <?php echo $captcha; ?>
                <div class="buttons clearfix">
                  <div class="pull-right">
                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                  </div>
                </div>
                <?php } else { ?>
                <?php echo $text_login; ?>
                <?php } ?>
              </form>
            </div>
            <?php } ?>
          </div>
        </div>
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-4'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
         <?php /*
          <div class="btn-group">
            <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="fa fa-heart"></i></button>
            <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');"><i class="fa fa-exchange"></i></button>
          </div>
         */ ?>
          <h1><?php echo $heading_title; ?></h1>
          <ul class="list-unstyled">
            <?php if ($manufacturer) { ?>
            <li><?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
            <?php } ?>
            <li><?php echo $text_model; ?> <?php echo $model; ?></li>
            <?php if ($reward && 0) { ?>
            <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
            <?php } ?>
            <li><?php echo $text_stock; ?> <?php echo $stock; ?></li>
          </ul>
          <?php if ($price) { ?>
          <ul class="list-unstyled">
            <?php if (!$special) { ?>
            <li>
              <h2><?php echo $price; ?></h2>
            </li>
            <?php } else { ?>
            <li><span style="text-decoration: line-through;"><?php echo $price; ?></span></li>
            <li>
              <h2><?php echo $special; ?></h2>
            </li>
            <?php } ?>
            <?php if ($tax) { ?>
            <li><?php echo $text_tax; ?> <?php echo $tax; ?></li>
            <?php } ?>
            <?php if ($points) { ?>
            <li><?php echo $text_points; ?> <?php echo $points; ?></li>
            <?php } ?>
            <?php if ($discounts) { ?>
            <li>
              <hr>
            </li>
            <?php foreach ($discounts as $discount) { ?>
            <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php } ?>
          <div id="product">
            <?php if ($options) { ?>
            <hr>
            <h3><?php echo $text_option; ?></h3>
            <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'select') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio">
                  <label>
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php if ($option_value['image']) { ?>
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                    <?php } ?>                    
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php if ($option_value['image']) { ?>
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                    <?php } ?>
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'text') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group date">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group datetime">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php if ($recurrings) { ?>
            <hr>
            <h3><?php echo $text_payment_recurring; ?></h3>
            <div class="form-group required">
              <select name="recurring_id" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($recurrings as $recurring) { ?>
                <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                <?php } ?>
              </select>
              <div class="help-block" id="recurring-description"></div>
            </div>
            <?php } ?>
            <div class="form-group">
              <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
              <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <br />
              
            </div>
            <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
            <?php } ?>
          </div>
          <?php if ($review_status) { ?>
          <div class="rating">
            <p>
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($rating < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } ?>
              <?php } ?>
              <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a></p>
            <hr>
            <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style" data-url="<?php echo $share; ?>"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
            <!-- AddThis Button END -->
          </div>
          <?php } ?>
        </div>
      </div>
      <?php if ($products) { ?>
      <h3><?php echo $text_related; ?></h3>
      <div class="row">
        <?php $i = 0; ?>
        <?php foreach ($products as $product) { ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-xs-8 col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-xs-6 col-md-4'; ?>
        <?php } else { ?>
        <?php $class = 'col-xs-6 col-sm-3'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
          <div class="product-thumb transition">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div class="caption">
              <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
              <p><?php echo $product['description']; ?></p>
              <?php if ($product['rating']) { ?>
              <div class="rating">
                <?php for ($j = 1; $j <= 5; $j++) { ?>
                <?php if ($product['rating'] < $j) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } ?>
                <?php } ?>
              </div>
              <?php } ?>
              <?php if ($product['price']) { ?>
              <p class="price">
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                <?php } ?>
                <?php if ($product['tax']) { ?>
                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>
            </div>
            <div class="button-group">
              
              <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
            </div>
          </div>
        </div>
        <?php if (($column_left && $column_right) && (($i+1) % 2 == 0)) { ?>
        <div class="clearfix visible-md visible-sm"></div>
        <?php } elseif (($column_left || $column_right) && (($i+1) % 3 == 0)) { ?>
        <div class="clearfix visible-md"></div>
        <?php } elseif (($i+1) % 4 == 0) { ?>
        <div class="clearfix visible-md"></div>
        <?php } ?>
        <?php $i++; ?>
        <?php } ?>
      </div>
      <?php } ?>
      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<?php echo $footer; ?>
