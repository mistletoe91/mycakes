<?php

$configMaster = array (
    'local.cakedelivery.ca' => array( 
                          'http'=>'http',
                          'https'=>'http',        
                          'base_url'=>'local.cakedelivery.ca', 
                          'base_dir'=>'C:/Users/IBM_ADMIN/Documents/xampp/htdocs/jas/mycakes'
                        ),
    'test.cakedelivery.ca' => array( 
                          'http'=>'http',
                          'https'=>'https',        
                          'base_url'=>'test.cakedelivery.ca', 
                          'base_dir'=>'/var/www/html/test.cakedelivery.ca'
                        ),     
    'www.cakedelivery.ca' => array( 
                          'http'=>'http',
                          'https'=>'https',        
                          'base_url'=>'www.cakedelivery.ca', 
                          'base_dir'=>'/var/www/html/cakedelivery.ca'
                        ),  
    'cakedelivery.ca' => array( 
                          'http'=>'http',
                          'https'=>'https',        
                          'base_url'=>'www.cakedelivery.ca', 
                          'base_dir'=>'/var/www/html/cakedelivery.ca'
                        )    
);


#
 
// HTTP
define('HTTP_SERVER', $configMaster [$_SERVER['SERVER_NAME']]['http'].'://'.$configMaster [$_SERVER['SERVER_NAME']]['base_url'].'/');

// HTTPS
define('HTTPS_SERVER', $configMaster [$_SERVER['SERVER_NAME']]['https'].'://'.$configMaster [$_SERVER['SERVER_NAME']]['base_url'].'/');

// DIR
define('DIR_APPLICATION', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/catalog/');
define('DIR_SYSTEM', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/');
define('DIR_IMAGE', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/image/');
define('DIR_LANGUAGE', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/catalog/language/');
define('DIR_TEMPLATE', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/catalog/view/theme/');
define('DIR_CONFIG', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/config/');
define('DIR_CACHE', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/storage/cache/');
define('DIR_DOWNLOAD', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/storage/download/');
define('DIR_LOGS', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/storage/logs/');
define('DIR_MODIFICATION', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/storage/modification/');
define('DIR_UPLOAD', $configMaster [$_SERVER['SERVER_NAME']]['base_dir'].'/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'cakedelivery');
define('DB_PASSWORD', 'Qwert1357!');
define('DB_DATABASE', 'cakedelivery');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
