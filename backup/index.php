<?php
if(isset($_GET['stripe"_card_token']) && $_GET['stripe_card_token']){
    include_once "controller".DIRECTORY_SEPARATOR."stripe_confiration.php";
}#endif
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Sticky Footer Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/sticky-footer-navbar.css" rel="stylesheet">
     
    <link href="/css/homepage.css" rel="stylesheet">
    <link href="/css/flipcard.css" rel="stylesheet">  
    <link rel="stylesheet" href="/css/nice-select.css">
    
    <link href="//fonts.googleapis.com/css?family=Roboto:400,300" rel="stylesheet" type="text/css" />
      
    <link href="/css/custom.css" rel="stylesheet">
 
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
 
  </head>

  <body> 
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">MyCakes</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

<!-- landing container start -->      
<div class="container twocolone  ">
<div class="row">
<main class="bs-docs-masthead my-conta1iner"   >
<div class="row">
  <div class="col-md-6">
    <div class="">
 <div class="starter-template">
<h1 class="page-header"><span> Order Cakes Online !</span></h1>
     <p class="lead">We made it simple to order and track <br> + 
         
<a class="no_underline_on_hover" data-toggle="tooltip" data-placement="right" title="Shipping is free in Toronto, Vaughan, Markham, Oakville, Mississauga, Caledon, Brampton, Burlington, Halton Hills, Milton, Richmond Hill and King"> 
                            FREE Delivery in 12 cities
                        </a>        </p> 
 
</div>
    </div>   
  </div>
  <div class="col-md-6"> 
      <img src="https://placehold.it/150X100"  style="width:100%;margin-top: 30px;"  /> 
      <?php /*
      
      
      <img src="images/cake_main.svg" style="width:100px;margin-left:40px;"/>
      <br />
      <img src="images/van.svg" style="width:200px;" />
      */ ?>
  </div>
</div> 
</main>  
  </div>
  <!-- end row -->
   

</div>
<!-- end landing container -->

      
      
    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <h1>Cake Order Form</h1> 
          
      </div> 
   

      
<!-- Content Start ********************************************************************************************** -->
  <div class="row" style="margin-top:20px; ">
  	<div class="col-md-8">  
        
        <?php /*
        <div class="hide">
    	    <canvas id="canvas-product-front" width="700" height="300"></canvas>          
          <div class="btn-group " role="group" aria-label="..."> 
            <button type="button" id="quater_slab" class="btn btn-default">1/4 Slab (Serves 15-20) $23.99 </button>
            <button type="button" id="half_slab" class="btn btn-default">1/2 Slab (Serves 30-40) $49.99 </button>
            <button type="button" id="full_slab" class="btn btn-default">Full Slab (Serves 70-90) $65.99</button>
          </div>
        </div>
        */ ?>
        
  <div class="panel-group accordionclass"  id="accordion1" role="tablist" aria-multiselectable="true">   
       
 
<div class="panel panel-default" id="divaccordion_1">
    <div class="panel-heading" role="tab" id="heading1_1">
      <h4 class="panel-title">
        <a id="collapse1_1_btn" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_1" aria-expanded="true" aria-controls="collapse1_1">
          Cake Order Form
        </a>
      </h4>
    </div>
    <div id="collapse1_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        
        <div class="row">  
            <div class="col-md-6">   
                  <p>Select Size</p>
                  <div class="mainselect_div"> 
                    <select> 
                        <option value="23.99">1/4 Slab (Serves 15-20) $23.99</option>
                        <option value="49.99">1/2 Slab (Serves 30-40) $49.99</option>
                        <option value="65.99">Full Slab (Serves 70-90) $65.99</option>
                    </select>                       
                  </div>
                
                  <p>Select Type</p>
                  <div class="mainselect_div"> 
                    <select> 
                        <option>Chocolate Cake</option>
                        <option>White Cake</option> 
                    </select>                       
                  </div>                

                  <p p>Select Color</p>
                  <div class="mainselect_div"> 
                    <select>
                        
                        <option  >White</option>
                        <option  >Brown</option>
                        <option  >Burgundy</option>
                        <option  >Green</option>
                        <option  >Holiday Red</option>
                        <option  >Lavender</option>
                        <option  >Mauve</option>
                        <option  >Orange</option>
                        <option  >Pink (Fuschia)</option>
                        <option  >Purple</option>
                        <option  >Red</option>
                        <option  >Royal Blue</option>
                        <option  >Sky Blue</option> 
                        <option  >Violet</option> 
                        <option  >Yellow</option> 
                        <option  >White</option> 
                        <option  >White</option> 
                    </select>                       
                  </div>        
                
                  <p>Select Decoration</p>
                  <div class="mainselect_div"> 
                    <select>
                        <option  >None</option>
                        <option  >One Happy Birthday Sign</option>
                        <option  >Two Clown Heads</option>
                        <option  >Red Ribbon</option>
                        <option  >One Balloon Cluster</option>
                        <option  >Foil Presents</option>
                        <option  >Blue Ribbon</option>
                        <option  >Silver Ribbon</option>
                        <option  >Lavender Ribbon</option> 
                        <option  >Yellow Ribbon</option>  
                    </select>                       
                  </div>         
                
                
            </div>
            <div class="col-md-6">   
              
                <div class="[ info-card ]">
					<img   src="/images/cake.svg" alt="Cake" />
					<div class="[ info-card-details ] animate">
						<div class="[ info-card-header ]">
							<h1> Order Your Cake  </h1>
							<h3> Free Delivery in GTA within 24-48 hrs  </h3>
						</div>
						<div class="[ info-card-detail ]">
							<!-- Description -->
							<p>The idea of creating something out of nothing has always generated a passion in my heart.  </p>
							<div class="social"> 
							</div>
						</div>
					</div>
				</div>                
                
                
                
            </div>            
        </div>       
         
        <div class="row">
            <div class="col-md-12 btnrow">    
                <a id="href_1_1" href="#" class="href_1 btn   btn-primary"><span class="glyphicon glyphicon-share-alt"></span> Next - Customize Cake</a>
            </div>
        </div>
           
          
                    
          <?php /*
        <div class="row overallbox" style=""> 
          <div class="col-md-4"> 
              <a id="href_1_1" class="href_1" href="#"><i  style="font-size: 80px;" class="fa fa-battery-quarter" aria-hidden="true"></i></a>
              <div class="hrefval"></div>
          </div>
          <div class="col-md-4"> 
              <a id="href_1_2" class="href_1" href="#"><i style="font-size: 80px;" class="fa fa-battery-half" aria-hidden="true"></i></a>
              <div class="hrefval"></div>
          </div>
          <div class="col-md-4"> 
              <a id="href_1_3" class="href_1" href="#"><i  style="font-size: 80px;" class="fa fa-battery-full" aria-hidden="true"></i> </a>
              <div class="hrefval"></div>
          </div> 
        </div>
        */ ?>
    
      </div>
    </div>
  </div>
      
      <?php /*
  <div class="panel panel-default" id="divaccordion_2">
    <div class="panel-heading" role="tab" id="heading1_2">
      <h4 class="panel-title">
        <a id="collapse1_2_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_2" aria-expanded="false" aria-controls="collapse1_2">
         Choose Cake Type
        </a>
      </h4>
    </div>
    <div id="collapse1_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
          
        <div class="row overallbox" style=""> 
          <div class="col-md-6"> 
              <a id="href_2_1" class="href_2" href="#"><i  style="font-size: 80px;" class="fa fa-battery-quarter" aria-hidden="true"></i></a>
              <div class="hrefval">Chocolate Cake</div>
          </div>
          <div class="col-md-6"> 
              <a id="href_2_1" class="href_2" href="#"><i style="font-size: 80px;" class="fa fa-battery-half" aria-hidden="true"></i></a>
              <div class="hrefval">White Cake</div>
          </div> 
        </div> 
          
      </div>
    </div>
  </div>
  */ ?>
  <div class="panel panel-default" id="divaccordion_2">
    <div class="panel-heading" role="tab" id="heading1_2">
      <h4 class="panel-title">
        <a id="collapse1_2_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_2" aria-expanded="false" aria-controls="collapse1_2">
          Choose Cake Filing
        </a>
      </h4>
    </div>
    <div id="collapse1_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
 
        <div class="row">  
            <div class="col-md-4">  
                <div class="funkyradio">  
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio22" checked /><label for="radio22">Chocolate Filling</label></div>
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio21" /><label for="radio21">Chantilly Filling</label></div>                    
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio23"  /><label for="radio23">Chocolate Fudge</label></div>      
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio24"  /><label for="radio24">White Icing</label></div>  
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio25"  /><label for="radio25">Chocolate Fudge</label></div>
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio26"  /><label for="radio26">Chocolate Icing</label></div>
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio27"  /><label for="radio27">Lemon Filling</label></div>                    
                  
                </div>
            </div>
            <div class="col-md-4">  
                <div class="funkyradio">  

                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio28"  /><label for="radio28">Whipped Topping</label></div>                    
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio29"  /><label for="radio29">Custard Filling</label></div>                 
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio211"  /><label for="radio211">Mocha  Filling</label></div>
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio212"  /><label for="radio212">Fresh Blueberries +$10.00</label></div>
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio213"  /><label for="radio213">Fresh Raspberries +$10.00</label></div>     
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio214"  /><label for="radio214">Fresh Kiwi+$10.00</label></div>
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio215"  /><label for="radio215">Fresh Strawberries +$10.00</label></div>                    
                </div>
            </div>
            <div class="col-md-4">  
                <div class="funkyradio">    

                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio216"  /><label for="radio216">Cherry Filling +$10.00</label></div>
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio217"  /><label for="radio217">Cream Cheese +$10.00</label></div>  
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio218"  /><label for="radio218">Mandarin Oranges +$10.00</label></div>
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio219"  /><label for="radio219">Peaches +$10.00</label></div>
                    <div class="funkyradio-primary"><input type="radio" name="radio" id="radio220"  /><label for="radio220">Pineapple +$10.00</label></div>                    
                </div>
            </div>            
        </div>   
        <!-- End Row -->
          
      </div>
    </div>
  </div>      
  <div class="panel panel-default" id="divaccordion_3">
    <div class="panel-heading" role="tab" id="heading1_3">
      <h4 class="panel-title">
        <a id="collapse1_3_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_3" aria-expanded="false" aria-controls="collapse1_3">
          Choose Cake Icining
        </a>
      </h4>
    </div>
    <div id="collapse1_3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <div class="row">
            <div class="col-md-4">  
                <div class="funkyradio"> 
                     <div class="funkyradio-primary"><input type="radio" name="radio3" id="radio31" checked/><label for="radio31">Chocolate Icing</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio3" id="radio32"  /><label for="radio32">Chocolate Fudge</label></div> 
                </div>
            </div>
 
            <div class="col-md-4">  
                <div class="funkyradio">   
                     <div class="funkyradio-primary"><input type="radio" name="radio3" id="radio33"  /><label for="radio33">Mocha  Icing</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio3" id="radio34"  /><label for="radio34">Whipped Topping</label></div> 
                </div>
            </div>
            <div class="col-md-4">  
                <div class="funkyradio">  
                     <div class="funkyradio-primary"><input type="radio" name="radio3" id="radio35"  /><label for="radio35">White Icing</label></div> 
                </div>
            </div>            
         </div>
        <!-- End Row -->
          
      </div>
    </div>
  </div>
   
      <?php /*
  <div class="panel panel-default" id="divaccordion_5">
    <div class="panel-heading" role="tab" id="heading1_5">
      <h4 class="panel-title">
        <a id="collapse1_5_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_5" aria-expanded="false" aria-controls="collapse1_5">
          Trim Colour
        </a>
      </h4>
    </div>
    <div id="collapse1_5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1_5">
      <div class="panel-body">
        <div class="row">
           <div class="col-md-4">  
                <div class="funkyradio">  
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio311" checked/><label for="radio311">White</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio312"  /><label for="radio312">Brown</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio313"  /><label for="radio313">Burgundy</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio314"  /><label for="radio314">Green</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio315"  /><label for="radio315">Holiday Red</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio316"  /><label for="radio316">Lavender</label></div>
                </div>
           </div>
           <div class="col-md-4">  
                <div class="funkyradio">   
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio317"  /><label for="radio317">Mauve</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio318"  /><label for="radio318">Orange</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio319"  /><label for="radio319">Pink (Fuschia)</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio3111"  /><label for="radio3111">Purple</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio3112"  /><label for="radio3112">Red</label></div>
                </div>
           </div>
           <div class="col-md-4">  
                <div class="funkyradio">                                      
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio3113"  /><label for="radio3113">Royal Blue</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio3114"  /><label for="radio3114">Sky Blue</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio3115"  /><label for="radio3115">Violet</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio6" id="radio3116"  /><label for="radio3116">Yellow</label></div> 
                </div>
            </div>   
        </div>
        <!-- End Row -->             
          
      </div>
    </div>
  </div>
 
      
  <div class="panel panel-default" id="divaccordion_6">
    <div class="panel-heading" role="tab" id="heading1_6">
      <h4 class="panel-title">
        <a id="collapse1_6_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_6" aria-expanded="false" aria-controls="collapse1_6">
          Choose Decoration
        </a>
      </h4>
    </div>
    <div id="collapse1_6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1_6">
      <div class="panel-body">
        <div class="row">
            <div class="col-md-4">  
                <div class="funkyradio">  
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio70" checked/><label for="radio70">None</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio71"  /><label for="radio71">One Happy Birthday Sign</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio72"  /><label for="radio72">Two Clown Heads</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio77"  /><label for="radio77">Red Ribbon</label></div>
                </div>
            </div>
            <div class="col-md-4">  
                <div class="funkyradio">  
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio73"  /><label for="radio73">One Balloon Cluster</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio74"  /><label for="radio74">Foil Presents</label></div>

                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio79"  /><label for="radio79">Blue Ribbon</label></div>
                </div>
            </div>
            <div class="col-md-4">  
                <div class="funkyradio">  
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio75"  /><label for="radio75">Silver Ribbon</label></div>
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio76"  /><label for="radio76">Lavender Ribbon</label></div>                     
                     <div class="funkyradio-primary"><input type="radio" name="radio7" id="radio78"  /><label for="radio78">Yellow Ribbon</label></div>
                </div>
            </div>            
         </div>
        <!-- End Row -->      
          
      </div>
    </div>
  </div>   
   */ ?>
      
  <div class="panel panel-default" id="divaccordion_7">
    <div class="panel-heading" role="tab" id="heading1_7">
      <h4 class="panel-title">
        <a id="collapse1_7_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_7" aria-expanded="false" aria-controls="collapse1_7">
          Write Message On Cake
        </a>
      </h4>
    </div>
    <div id="collapse1_7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1_7">
      <div class="panel-body">
        <div class="row">&nbsp;</div>
        <div class="row">
            <div class="col-md-6">   
                  <div class="input-group">      
                    <input  type="text"  required >
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label  class="fancytextinputlabels">Message Line 1</label>
                  </div>
                    
                  <div class="input-group">      
                    <input   type="text"  required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Message Line 2</label>
                  </div> 
                 
                  <div class="input-group">      
                    <input   type="text"  required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Message Line 3</label>
                  </div>              
            </div>   
            
            <div class="col-md-6">   
              
                <div class="[ info-card ]">
					<img   src="/images/cake.svg" alt="Cake" />
					<div class="[ info-card-details ] animate">
						<div class="[ info-card-header ]">
							<h1> Write custom Message  </h1>
							<h3> upto 3 lines 25 char each </h3>
						</div>
						<div class="[ info-card-detail ]">
							 Description 
							<p>The idea of creating something out of nothing has always generated a passion in my heart.  </p>
							<div class="social"> 
							</div>
						</div>
					</div>
				</div>                
                
                
                
            </div>

            
         </div>
        <!-- End Row -->
          
        <div class="row">
            <div class="col-md-12 btnrow">     
                <a id="href_delivery_details" href="#" class="btn  btn-primary"><span class="glyphicon glyphicon-share-alt"></span> Next - Delivery Details</a>
            </div>
        </div>          
          
      </div>
    </div>
  </div>       
       
  <div class="panel panel-default" id="divaccordion_8">
    <div class="panel-heading" role="tab" id="heading1_8">
      <h4 class="panel-title">
        <a id="collapse1_8_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_8" aria-expanded="false" aria-controls="collapse1_8">
          Delivery Details
        </a>
      </h4>
    </div>
    <div id="collapse1_8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1_8">
      <div class="panel-body">
        <div class="row">&nbsp;</div>
        <div class="row">
            <div class="col-md-6">    
                  <div class="input-group">      
                    <input id="autocomplete" placeholder="Start Typing Your Address" onFocus="geolocate()" type="text"></input>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label  class="fancytextinputlabels">Delivery Address</label>
                  </div>
                    
                
 <table id="address" class="hide">
      <tr>
        <td class="label">Street address</td>
        <td class="slimField"><input class="field" id="street_number"
              disabled="true"></input></td>
        <td class="wideField" colspan="2"><input class="field" id="route"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">City</td>
        <!-- Note: Selection of address components in this example is typical.
             You may need to adjust it for the locations relevant to your app. See
             https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete-addressform
        -->
        <td class="wideField" colspan="3"><input class="field" id="locality"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">Postal</td>
        <td class="slimField"><input class="field"
              id="postal_code" disabled="true"></input></td>
    
      </tr>        
      <tr>
        <td class="label">State</td>
        <td class="slimField"><input class="field"
              id="administrative_area_level_1" disabled="true"></input></td>
    
      </tr>
      <tr>
        <td class="label">Country</td>
        <td class="wideField" colspan="3"><input class="field"
              id="country" disabled="true"></input></td>
      </tr>
    </table>  
                  <div class="input-group">      
                    <input   type="text"  required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Delivery Instructions e.g. Buzzer#</label>
                  </div> 
                                 
                 
            </div>   

            <div class="col-md-6">   
              
                <div class="[ info-card ]">
					<img   src="/images/map.png" alt="Security Seal" />
					<div class="[ info-card-details ] animate">
						<div class="[ info-card-header ]">
							<h1> Always FREE Delivery  </h1>
							<h3> We make sure to deliver fast </h3>
						</div>
						<div class="[ info-card-detail ]">
							<!-- Description -->
							<p>The idea of creating something out of nothing has always generated a passion in my heart.  </p>
							<div class="social"> 
							</div>
						</div>
					</div>
				</div>                
                
                
                
            </div>

         </div>
        <!-- End Row -->       

        <div class="row">
            <div class="col-md-12 btnrow">     
                <button id="href_payment_details" disabled  type="button" class="btn btn-primary"><span class="glyphicon glyphicon-share-alt"></span> Final Step - Payment Information</button>
                <div id="div_deliverymsg"></div>
            </div>
        </div>
          
      </div>
    </div>
  </div>         
<!-- end Panel -->      
      
   
        
  <div class="panel panel-default" id="divaccordion_9">
    <div class="panel-heading" role="tab" id="heading1_9">
      <h4 class="panel-title">
        <a id="collapse1_9_btn" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1_9" aria-expanded="false" aria-controls="collapse1_9">
          Payment Details
        </a>
      </h4>
    </div>
    <div id="collapse1_9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1_9">
      <div class="panel-body">
        <div class="row">&nbsp;</div>
        <div class="row">
            <div class="col-md-6">   
                
<form role="form" id="payment-form" method="POST" action="javascript:void(0);">
  
    
                  <div class="input-group">      
                      <input 
                                            type="tel"
                                            class="payment-field"
                                            name="cardNumber" 
                                            required autofocus 
                                        />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Credit Card#</label>
                  </div>   
    
                  <div class="input-group">      
                     <input name="cardExpiry" type="tel" class="payment-field"   required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Expiry (mm/yyyy)</label>
                  </div>       
    
                  <div class="input-group">      
                   <input name="cardCVC" type="tel" class="payment-field"   required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">CVC</label>
                  </div>     
    <?php /*
                  <div class="input-group">      
                    <input   type="text"   name="cardholder-name"  required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Billing Name</label>
                  </div>
    
                  <div class="input-group">      
                    <input   type="text"   name="telephone_payment"  required>
                    <input   type="tel"   style="display:none;" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Phone</label>
                  </div>     
    
                  <div class="input-group">      
                    <input   type="text" name="address-zip"  required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels">Billing Postal Code</label>
                  </div>   
    */ ?>
     
    
  <?php /*  
                 <div class="input-group"  style="border-bottom:1px solid #00BFFF;">       
                    <div id="card-element" class="field"></div>  
                    <span class="highlight">Card#</span>
                    <span class="bar"></span>
                    <label class="fancytextinputlabels"></label>
                  </div> 
                  
  <label>
    <span>Name</span>
    <input name="cardholder-name" class="field" placeholder="Jane Doe" />
  </label>
  <label>
    <span>Phone</span>
    <input class="field" placeholder="(123) 456-7890" type="tel" />
  </label>
  <label>
    <span>ZIP code</span>
    <input name="address-zip" class="field" placeholder="94110" />
  </label>
  <label>
    <span>Card</span>
    <div id="card-element" class="field"></div>
  </label>
  */ ?>
    
                        <div class="row">
                            <div class="col-xs-12">
                                
                                <table class="paymenttable table-bordered table-condensed"> 
                                    <tr ><td class="info"><h4>Final Price</h4></td></tr>
                                  <tr> 
                                      <td>$123.23 + $11.23 (HST) = <strong>$131.23</strong></td>
                                  </tr>  
                                    <tr><td  style="font-size:13px;">Delivery is Free and estimated time <br />is between 24-48 hours</td></tr>
                                </table>
                                
                            </div>
                        </div>    
    
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="subscribe btn btn-success btn-lg btn-block" type="button">Pay $131.23</button>
                            </div>
                        </div>
                        <div class="row" style="display:none;">
                            <div class="col-xs-12">
                                <p class="payment-errors"></p>
                            </div>
                        </div>  
 
</form>
                
            </div>     
            <div class="col-md-6">   
              
                <div class="[ info-card ]">
					<img   src="/images/security_seal.png" alt="Security Seal" />
					<div class="[ info-card-details ] animate">
						<div class="[ info-card-header ]">
							<h1> Seucure Website  </h1>
							<h3> We do not secure your CC info </h3>
						</div>
						<div class="[ info-card-detail ]">
							<!-- Description -->
							<p>The idea of creating something out of nothing has always generated a passion in my heart.  </p>
							<div class="social"> 
							</div>
						</div>
					</div>
				</div>                
                
                
                
            </div>
         </div>
        <!-- End Row -->              
          
      </div>
    </div>
  </div>  

        


        
  


  


 
        </div> <!-- end of main accordian --> 
  	</div><!-- END col-md-8 -->
  <div class="col-md-4">
  	
  	
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"> Final Price</h3> 
				</div>
                <div class="panel-collapse collapse in">
				    <div class="panel-body" style="text-align:center;">
                        <h1 class="placeholder_finalprice">$123.31<small>+ HST</small></h1>
                        
                                                  
                                
                        <a class="no_underline_on_hover" data-toggle="tooltip" data-placement="right" title="Shipping is free in Toronto, Brampton, Mississauga, Caledon, Oakville, Markham, Milton, Scarborough, Vaughan, Brampton etc."> 
                            FREE Shipping
                        </a>
                    </div>
                </div>
			</div>
      
      <?php /*
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingZero">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Final Price
          

                      
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingZero">
      <div class="panel-body" style="text-align:center;"> 
        
 	        <h1 class="placeholder_finalprice">$123.31<small>+ HST</small></h1> 
         
<div style="display:none">         
<div style="display: inline-block; "> 
  <p>
    <label>
      <code>padding</code>
      <input id="padding" value="0" min="0" max="50" style="display: block" type="range">
    </label>
  </p> 
</div>      
</div>
         
      </div>
    </div>
  </div> 
       
      
     
 
  <div class="panel panel-default hide">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Nothing here
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body"> 
      </div>
    </div>
  </div>  
</div>  	
  	 */ ?>
    
    
  	</div>
  </div>
 
      
 <!-- Portfolio -->
<div class="row" >
  <div class="starter-template">
        <h1 class="page-header">Why Chose Our   <span>Cakes ?</span></h1>
        <p class="lead">You will get all of the following regardless. So why don`t you sign up today ? Just do it </p>
  </div>

    <div class="row stylish-panel">
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-cloud  insideicons"  ></i>
             </div>
           </div>
           <h2>While-label Service</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div>
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-television  insideicons" ></i>
             </div>
           </div>
           <h2>Easy-Embedd Widget</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div>
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-mobile-phone insideicons" ></i>
             </div>
           </div>
           <h2>Mobile App</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div>
    </div>

    <div class="row stylish-panel">
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-shield insideicons"  ></i>
             </div>
           </div>
           <h2>Robust Security</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management .
           </p>
        </div>
      </div>
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-hand-o-up insideicons" ></i>
             </div>
           </div>
           <h2>Control Document Transfer</h2>
           <p class="moreinfotext">This service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div> 
      <div class="col-md-4">
        <div>
           <div class="insideicon">
             <div class="circle circle-blue">
               <i class="fa fa-thumb-tack insideicons" ></i>
             </div>
           </div>
           <h2>Tracking</h2>
           <p class="moreinfotext">TrThis service is made under guidance of Canadian Law regarding record management
           </p>
        </div>
      </div>
    </div>
    <div style="Clear:both"></div>
</div>
<div style="Clear:both"></div>
<!-- /Portfolio -->



            </div>
        </div>
    </div>      
<!-- content End *************************************************************************************************** --> 
      
 </div>
      
    <footer class="footer">
      <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script> 

    <script src="https://use.fontawesome.com/5765c0aedd.js"></script>

    <script src="/fabricjs/fabric.min.js" type="text/javascript"></script>  
    <script src="/js/jquery.validate.min.js" type="text/javascript"></script>  
    <script src="/js/jquery.sticky.js" type="text/javascript"></script>   

    <script src="/js/jquery.nice-select.min.js"></script>

    <script src="/js/google_auto_address.js" type="text/javascript"></script>      
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkdhV1l28IQEqvFbpxeQuYQkYpMnPotmM&libraries=places&callback=initAutocomplete" async defer></script>
 
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.2.3/jquery.payment.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script src="/js/payment1.js" type="text/javascript"></script>          
    
    <script src="/js/custom.js" type="text/javascript"></script>      
  </body>
</html>
